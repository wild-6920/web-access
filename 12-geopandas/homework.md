# GeoPandas homework

**Due April 5 at midnight**

## Notebook comprehension questions

*Worth 4 points*

Sorry, but everything is in one long notebook this week...

- 1-geopandas.ipynb


## Script problems

*Worth 10 points each*

## Problem 1

Create a notebook called `problem1`. Set up four variables in the very first cell, with these names and values (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\WILD6920\Spring2020\python-gis\12-geopandas\data'
city_filename = 'cities.shp'
county_filename = 'county.shp'
stats_filename = 'stats.csv'
```

Give this cell a `parameters` tag.

Your notebook needs to create a csv file (`stats_filename`) that contains statistics for the POPLASTCEN field in the city file, but grouped by county name.

Include these statistics for each county:

- minimum
- maximum
- mean
- standard deviation

I want you to include the county name in the output, so you'll need to join the city and county files together just like the notebook does. Make sure the name column is called 'COUNTY' in your output.

Have your notebook show the results before it saves them to the csv file. 

Make sure that `check_homework.py` is in the same folder as your notebook and call the `problem1()` function in that file. You'll have to import it as a module first, and pass `stats_filename` to the function when you call it. It'll do some basic checking on your output file. (`check_homework.py` is in this week's folder.)

Make sure that your notebook runs if you restart the kernel and that you've used the variables from the first cell rather than typing out the filenames later on.

The first few rows of your result should look like this, and there should be 29 rows total:

![problem 1 results](images/p1.png)


## Problem 2

Create a notebook called `problem2`. Set up four variables in the very first cell, with these names and values (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\WILD6920\Spring2020\python-gis\12-geopandas\data'
sites_filename = 'sites.shp'
output_filename = 'trees.shp'
cover_type = 'trees'
```

Give this cell a `parameters` tag.

Your notebook needs to create a new shapefile that contains all of the features from `sites_filename` that have a `COVER` value that matches `cover_type`. Save the new shapefile to `output_filename`. There's one twist, though. You need to add two new columns called `x` and `y` that contain the x and y coordinates for the points (sites.shp is a point shapefile). You saw how to create new columns, and you can get coordinates like `your_dataframe.geometry.x`, and the same thing for y. So use that to put the coordinate values into new columns. Make sure your notebook displays a few rows of the final dataframe so that you can make sure they look okay.

Either add the x and y columns before selecting the rows of interest, or use `my_dataframe.copy()` to make a copy of the selected rows before trying to add the x and y columns. Otherwise you'll get an error about trying to modify a subset of a dataframe.

Plot the new points on top of the original points (using GeoPandas, not classtools). Make the original points black and the new points red. You can set the colors by passing something like `color='black'` to `plot()`.

Make sure that `check_homework.py` is in the same folder as your notebook and call the `problem2()` function in that file. You'll have to import it as a module first, and pass `output_filename` to the function when you call it. It'll do some basic checking on your output file. (`check_homework.py` is in this week's folder.)

Make sure that your notebook runs if you restart the kernel and that you've used the variables from the first cell rather than typing out the filenames or cover type later on.

The first few rows of your new dataframe should look like this:

![problem 2 dataframe](images/p2_df.png)

And your plot should look like this:

![problem 2 plot](images/p2_plot.png)


## Problem 3

Create a notebook called `problem3`. Set up three variables in the very first cell, with these names and values (except change the path to the data folder on **your** computer:

```python
filename = r'D:\classes\WILD6920\Spring2020\python-gis\12-geopandas\data'
point_id = 375
distance = 200
```

Give this cell a `parameters` tag.

This notebook needs to find out how many deer locations in the shapefile are within a given distance of a specific location point (the one with the provided ID). Assume that there's a field in the shapefile called 'id'. For example, you'll test it by finding the number of features in deer.shp that are within 200 meters of the point with id=375. Print out the number of points that meet this condition.

Also plot all the points in black, the selected points in blue, and the the point with `point_id` in red. Use GeoPandas, not classtools.

If you use the buffer method and want more of a challenge, see if you can add the buffer outline, too. *Hint: There's an example in the notebook that draws the counties as just outlines, except that it uses white as the fill color. Try using 'none' (not None) instead.*

You can assume that the distance provided is in the same map units as the shapefile, so you don't need to worry about units. (The deer shapefile is UTM, so the map units are meters.) 

Make sure that your notebook runs if you restart the kernel and that you've used the variables from the first cell rather than typing out the filenames later on.

You should find 16 points and your plot should look like this:

![problem 3 result](images/p3.png)

There are several ways to go about this, and I've outlined a couple of them below. You can use whichever one you want. **Pay attention to the information here, because it contains important information that isn't in the notebook!**

### Method one: distance

This method has you get the geometry object out of the selected row and then calculate the distance between that point and all other points in the dataset. Then you select out the ones that are within `distance` and count them.

1. One way to get a single value (like a geometry) out of a dataframe is to use `data.at[row_name, column_name]`, where `row_name` is the index value for the row (like what we used with `loc`). How could you set up your data so that the Id values were used as the index so you could use the `point_id` variable with `at`?
2. Once you have a geometry, you can use [distance](https://geopandas.org/reference.html#geopandas.GeoSeries.distance) to create a new column in the dataframe that contains the distance from each point to that specific geometry.
3. Once you have a column with distances, you can select out the ones that fit your criteria and count them (the very beginning of the geopandas notebook shows you how to count the rows).

### Method two: buffer

This method has you buffer the geometry of interest and then use a spatial join to find the points that fall within a buffer (`overlay` won't work with points).

1. Make a new dataframe that is limited to the point with `point_id`. Make sure it's still a dataframe instead of a series (if you print it out, it'll print as a single row with multiple columns rather than lots of rows). You want to keep your original dataframe, too, so that you can join your buffered point to it later. You'll probably have to make a copy of it so that you don't get warnings later about trying to edit a slice from a dataframe. You can do that with `data = data.copy()`. 
2. Use [buffer](https://geopandas.org/reference.html#geopandas.GeoSeries.buffer) to create a new column in your limited dataframe that has the point buffered by the distance that you're interested in.
3. Change your limited dataframe so that it uses the new buffered column as the geometry column. You can do that with `data = data.set_geometry(name_of_new_geometry_column)`. Then get rid of the old geometry column, because otherwise you'll get errors when you try to join the two dataframes.
4. Now use a spatial join to join the buffered data to the point data (NOT the other way around, because it's the points you're interested in so you want them to be the main event).
5. After the spatial join, the points that didn't fall within the buffer will have null values for the columns that came from the buffered dataframe. Their names will end with `_right`. Choose one of them to use for the next step.
6. You can find rows that have non-null values for a column with `data[data.some_column.notna()]`, so find and count the rows that don't have null values for your chosen field. These are the ones that fall within the buffer. (The very beginning of the geopandas notebook shows you how to count the rows.)
