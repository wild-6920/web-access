import pandas as pd
import geopandas as gpd

def problem1(fn):
    df = pd.read_csv(fn)
    assert len(df) == 29, f'CSV has {len(df)} rows but it should have 29'
    cols = ['COUNTY', 'min', 'max', 'mean', 'std']
    assert set(df.columns) == set(cols), f'Columns should be {cols} but are {df.columns.values}'
    df = df.set_index('COUNTY')
    assert all(df.loc['CACHE', cols[1:]].values.round(2) == pd.array([259, 42670, 4506.58, 9486.83])), \
        'Statistics for Cache County look wrong'
    assert all(df.loc['UTAH', cols[1:]].values.round(2) == pd.array([-1, 105166, 15110.24, 25612.03])), \
        'Statistics for Utah County look wrong'
    print('Problem 1: All tests passed.')

def problem2(fn):
    df = gpd.read_file(fn)
    assert len(df) == 11, f'Shapefile has {len(df)} rows but it should have 11'
    cols = ['ID', 'COVER', 'Count', 'x', 'y', 'geometry']
    assert set(df.columns) == set(cols), f'Columns should be {cols} but are {df.columns.values}'
    assert all(df.COVER == 'trees'), 'What happened to the trees in the COVER column?'
    df = df.set_index('ID')
    assert all(df.loc[9, cols[2:-1]].values.astype('float').round(3) == pd.array([3, 477395.127, 4633169.387])), \
        'Values for row with ID 9 look wrong'
    assert all(df.loc[36, cols[2:-1]].values.astype('float').round(3) == pd.array([4, 462938.841, 4634541.151])), \
        'Values for row with ID 36 look wrong'
    assert df.crs['init'] == 'epsg:32612', 'What happened to the spatial reference?'
    print('Problem 2: All tests passed.')
