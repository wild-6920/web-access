{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "**Don't forget to change the path in this cell so that Python can find the datasets for this week.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Change this to point to your 14-graphics\\data folder.\n",
    "data_folder = r'D:\\classes\\WILD6920\\Spring2020\\python-gis\\14-graphics\\data'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from osgeo import gdal\n",
    "\n",
    "# Change the Python working directory.\n",
    "os.chdir(data_folder)\n",
    "\n",
    "# Set up matplotlib to draw in the notebook.\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Drawing rasters with matplotlib\n",
    "\n",
    "You've used the `imshow()` function in matplotlib to draw rasters, but only to draw one band at a time, and you've probably noticed that it draws large rasters pretty slowly. This will show you how to speed things up and how to use more than one band at a time.\n",
    "\n",
    "Yep, I know you ended up seeing some of this last week. It's a good review to see it again. :)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Drawing overviews\n",
    "\n",
    "Just as overviews (pyramid layers) allow ArcGIS (or QGIS, or anything else, for that matter) to draw a raster more quickly, you can use them to draw a raster faster with matplotlib. Of course, that requires that the raster has overviews in the first place.\n",
    "\n",
    "There's an NDVI image in your data folder, so open it up and read the pixel data into a numpy array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ndvi_ds = gdal.Open('ndvi.tif')\n",
    "ndvi_band = ndvi_ds.GetRasterBand(1)\n",
    "ndvi_data = ndvi_band.ReadAsArray()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's draw it without overviews (two different ways). According to the [documentation](https://matplotlib.org/api/_as_gen/matplotlib.pyplot.imshow.html), `imshow()` wants values in the range 0.0 - 1.0. It will automatically try to scale the values for you, but that doesn't work in this case because the -99 NoData values are included and they mess up the scale. Draw it in grayscale (`cmap='gray'`) and see for yourself:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(ndvi_data, cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a couple of ways you can fix this. First, you know that the NDVI has values that range from -1.0 to 1.0, so you can create a [Normalize](https://matplotlib.org/api/_as_gen/matplotlib.colors.Normalize.html) object that scales the data based on the min and max values you give it. You can pass that as the `norm` parameter to `imshow()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(ndvi_data, norm=plt.Normalize(-1, 1), cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The second method not only fixes the display, but also gets rid of the black around the edges. You know those are NoData pixels, so you can mask them out before plotting the data. This example gets the NoData value from `ndvi_band` rather than assuming that the NoData value is -99. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Mask out the NoData values.\n",
    "nodata = ndvi_band.GetNoDataValue()\n",
    "ndvi_data_masked = np.ma.masked_equal(ndvi_data, nodata)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now all of the non-masked values in the `ndvi_data_masked` array are actual NDVI values, so you can let matplotlib scale the data for you. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(ndvi_data_masked, cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It takes a couple of seconds to draw that, though, so let's see how to use overviews for drawing instead. \n",
    "\n",
    "You can find out how many overview levels a band has with it's `GetOverviewCount()` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ndvi_band.GetOverviewCount()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The last level is the coarsest, so let's get it it using the band's `GetOverview()` method. This wants the index of the overview to get, and the indexes start at 0. It returns a band object, but this particular band object contains overview data instead of the full band data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the index of the last (coarsest) overview (4, in this case).\n",
    "ov_index = ndvi_band.GetOverviewCount() - 1 \n",
    "\n",
    "# Get the overview.\n",
    "ov_band = ndvi_band.GetOverview(ov_index)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since `ov_band` is just a band object, you can read data in the same way you read data from any other band, by using `ReadAsArray()`, and you can plot that data just like any NumPy array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read the overview data and mask it.\n",
    "ov_nodata = ov_band.GetNoDataValue()\n",
    "ov_data = np.ma.masked_equal(ov_band.ReadAsArray(), ov_nodata)\n",
    "\n",
    "# Plot the overview data.\n",
    "plt.imshow(ov_data, cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That draws a lot faster! You can also see that it looks a bit different, because the overview levels were built as averages, which smooths things out a bit. There's probably a noticeable difference if you use overview level 2 instead..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ov_band = ndvi_band.GetOverview(2)\n",
    "ov_data = np.ma.masked_equal(ov_band.ReadAsArray(), ov_band.GetNoDataValue())\n",
    "plt.imshow(ov_data, cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that it still drew really fast. The original will still draw slowly, even though the data is already in memory:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(ndvi_data_masked, cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Standard deviation stretch\n",
    "\n",
    "You can also apply a standard deviation stretch in order to increase the contrast (ArcGIS does this by default). To do that, you need to figure out the bounding values that are two standard deviations above and below the mean pixel value. Then you use those as the new min and max values for Normalize, where everything smaller is treated as the min, and everything greater is treated as the max. Instead of applying the color ramp between -1 and 1, you apply it between these two numbers. This usually results in a better looking image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the mean and the standard deviation * 2.\n",
    "mean, d = ov_data.mean(), ov_data.std() * 2\n",
    "\n",
    "# Normalize based on a 2-standard deviation stretch.\n",
    "plt.imshow(ov_data, norm=plt.Normalize(mean - d, mean + d), cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "### Problem 1\n",
    "\n",
    "Plot `ov_data` using a 1-standard deviation stretch."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Color ramps\n",
    "\n",
    "There's a list of color ramps at http://matplotlib.org/examples/color/colormaps_reference.html. Go ahead and play with some of the other available color schemes (names are case-sensitive)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mean, d = ov_data.mean(), ov_data.std() * 2\n",
    "plt.imshow(ov_data, norm=plt.Normalize(mean - d, mean + d), cmap='BrBG')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "### Problem 2\n",
    "\n",
    "Plot `ov_data` using the winter colormap and a 2-standard deviation stretch."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You're done with the NDVI raster now, so go ahead and close it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "del ndvi_ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Multiple bands\n",
    "\n",
    "Now let's see how to plot multiple bands using the ASTER image. It doesn't have overviews yet, so let's build them first."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aster_ds = gdal.Open('aster.img')\n",
    "print('Number of overviews before:', aster_ds.GetRasterBand(1).GetOverviewCount())\n",
    "\n",
    "# Figure out what overviews to build up until the images are less then 256 pixels.\n",
    "levels = [2]\n",
    "while levels[-1] < max(aster_ds.RasterXSize, aster_ds.RasterYSize) / 256:\n",
    "    levels.append(levels[-1] * 2)\n",
    "aster_ds.BuildOverviews('average', levels)\n",
    "\n",
    "print('Number of overviews after:', aster_ds.GetRasterBand(1).GetOverviewCount())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now read in the coarse overviews for each band:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ov_index = aster_ds.GetRasterBand(1).GetOverviewCount() - 1\n",
    "green = aster_ds.GetRasterBand(1).GetOverview(ov_index).ReadAsArray()\n",
    "red = aster_ds.GetRasterBand(2).GetOverview(ov_index).ReadAsArray()\n",
    "nir = aster_ds.GetRasterBand(3).GetOverview(ov_index).ReadAsArray()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The band data need to be stacked into a 3D NumPy array in order to make it work. You need to stack the bands in the order that you want to use for RGB, so you'll use nir, red, green in order to get a color infrared composite. Once you've stacked them, just pass the stack to `imshow`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = np.dstack((nir, red, green))\n",
    "plt.imshow(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Multiband standard deviation stretch\n",
    "\n",
    "Unfortunately, stretching a multi-band image isn't as easy as a single band because there aren't any built-in functions to help you (at least not that I'm aware of), so you need to stretch the data manually. Here's a function that applies a standard deviation stretch to a NumPy array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "def std_stretch_data(data, n=2):\n",
    "    \"\"\"Applies an n-standard deviation stretch to data.\"\"\"\n",
    "    \n",
    "    # Get the mean and n standard deviations.\n",
    "    mean, d = data.mean(), data.std() * n\n",
    "    \n",
    "    # Calculate new min and max as integers. Make sure the min isn't \n",
    "    # smaller than the real min value, and the max isn't larger than \n",
    "    # the real max value.\n",
    "    new_min = math.floor(max(mean - d, data.min()))\n",
    "    new_max = math.ceil(min(mean + d, data.max()))\n",
    "    \n",
    "    # Convert any values smaller than new_min to new_min, and any\n",
    "    # values larger than new_max to new_max.\n",
    "    data = np.clip(data, new_min, new_max)\n",
    "    \n",
    "    # Scale the data.\n",
    "    data = (data - data.min()) / (new_max - new_min)\n",
    "    return data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use that function to stretch each band's data array individually. You don't want to stretch the alpha array because you don't want to change its 0 and 1 values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "green_stretched = std_stretch_data(green, 2)\n",
    "red_stretched = std_stretch_data(red, 2)\n",
    "nir_stretched = std_stretch_data(nir, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now stack and plot these stretched arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_stretched = np.dstack((nir_stretched, red_stretched, green_stretched))\n",
    "plt.imshow(data_stretched)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Much better!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Masking\n",
    "\n",
    "With multiple bands like this, you can't just mask out the data you don't want to see (like the edges). Instead, you need to change it so that you have four bands instead of three, where the fourth is an *alpha* band. Put a zero in this band for any pixel that you want to be transparent (so it won't draw) and a one in any pixel that you want to be fully opaque. If you wanted, you could make your image semi-transparent by using values between 0 and 1 in this band."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Everywhere that red, green, and nir are 0 gets a 0, and everywhere else gets a 1.\n",
    "alpha = np.where(red + green + nir == 0, 0, 1).astype(np.byte)\n",
    "\n",
    "# Stack all four bands and draw the result\n",
    "data_stretched = np.ma.dstack((nir_stretched, red_stretched, green_stretched, alpha))\n",
    "plt.imshow(data_stretched)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to get rid of the axes ticks, use `axis('off')`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.axis('off')\n",
    "plt.imshow(data_stretched)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "### Problem 3\n",
    "\n",
    "Plot the aster data using a 1-standard deviation stretch and a near infrared color composite (that's the band combo that we've been using already). Use the `red`, `green`, `nir`, and `alpha` arrays already in memory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
