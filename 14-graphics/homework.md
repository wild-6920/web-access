# Graphics homework

**Due April 19 at midnight**

If you're having trouble finding a computer to use, try it online with this link: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/wild-6920%2Fweb-access/master)

You know what? I'm exhausted and really far behind on grading (I really am sorry about that), and I know things are tougher for you these days, too. So I decided to do away with script problems this week and just have notebooks. Hopefully that's okay with you!

## Notebook comprehension questions

*Worth 10 points*

- 1-matplotlib.ipynb (4 points)
- 2-pandas.ipynb (2 points)
- 3-drawing-rasters.ipynb (4 point)

**Don't forget that your projects are due on the 23rd at 5pm!**
