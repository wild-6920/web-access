{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "**Don't change the data folder.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Don't change this.\n",
    "data_folder = 'data'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "import os\n",
    "import geopandas as gpd\n",
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from osgeo import gdal\n",
    "\n",
    "# Change the Python working directory.\n",
    "os.chdir(data_folder)\n",
    "\n",
    "# Set up matplotlib to draw in the notebook.\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You'll need the GDAL_DATA environment variable to be set for this, so run this code to set it in case it isn't already set. (I really need to figure out why it doesn't get set correctly with these environments! I never have to do this in my own code.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not os.environ.get('GDAL_DATA'):\n",
    "    print('Updating GDAL_DATA')\n",
    "    os.environ['GDAL_DATA'] = os.path.join(os.environ['CONDA_PREFIX'], 'Library', 'share', 'gdal')\n",
    "else:\n",
    "    print('Nothing to do')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Demo: Sample a raster at specific points\n",
    "\n",
    "This demo will show you how to sample a raster at points contained in a shapefile, but using GeoPandas and GDAL. You'll also learn a few new things about those packages along the way.\n",
    "\n",
    "First open the raster and get its dimensions. You saw how to do this in the [raster-basics](../11-gdal/1-raster-basics.ipynb) notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open aster.img and get its dimensions.\n",
    "ds = gdal.Open('aster.img')\n",
    "\n",
    "rows = ds.RasterYSize\n",
    "cols = ds.RasterXSize\n",
    "bands = ds.RasterCount\n",
    "rows, cols, bands"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read all bands into a single 3D array\n",
    "\n",
    "During the GDAL week you saw how to read a single band into a 2-dimensional array, but it's also possible to read all bands into a 3-dimensional array. To do that, use `ReadAsArray()` on the dataset rather than the band. Here's an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read the data.\n",
    "data = ds.ReadAsArray()\n",
    "\n",
    "# Get the shape of the 3D array.\n",
    "data.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first number in `shape` is the number of rows, the second is the number of columns, and the third is the depth of the array. It looks like it read things in the wrong order, because you only have 3 rows. There are two ways to read data from the raster, and the first is *band interleaved*. I'm not going to go into the different types of interleaving, but they have to do with how the data are stored in the file (or in this case, memory). If you have a newer version of GDAL, you can force it to read it how you expect by telling it to use *pixel interleaving*, like this (if it crashes, keep reading):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = ds.ReadAsArray(interleave='pixel')\n",
    "data.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If that crashed with a message about `an unexpected keyword argument 'interleave'`, then you have an older version of GDAL and you have to do it the hard way and read one band at a time and then combine them. \n",
    "\n",
    "This uses a list comprehension to read all of the bands in the raster individually and put them in a list, and then it uses the NumPy `dstack()` function to combine them into one 3D raster. If the code were written out the really long way, it would look like this:\n",
    "\n",
    "```python\n",
    "data_list = []\n",
    "for i in range(ds.RasterCount):\n",
    "    band = ds.GetRasterBand(i + 1)\n",
    "    band_data = band.ReadAsArray()\n",
    "    data_list.append(band_data)\n",
    "data = np.dstack(data_list)\n",
    "data.shape\n",
    "```       \n",
    "\n",
    "**You don't have to run this next cell if the last one worked, and you especially don't want to if you're using the online environment and things are running slowly.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = np.dstack([ds.GetRasterBand(i + 1).ReadAsArray() for i in range(ds.RasterCount)])\n",
    "data.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Whichever way you ran it,** this is much better. Now the numbers of rows and columns in the `data` array match the number of rows and columns in the raster, and the depth is 3 since there are three bands.\n",
    "\n",
    "Now that it's formatted like this, you can even use `imshow()` to plot it (although it won't be very pretty without doing a bunch of work to make it look better). But here it is: **(If it took forever to read the data, it will probably also take forever to plot it.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Speed up plotting\n",
    "\n",
    "Since `imshow()` runs so slow online, I'll show you something that I was going to show you next week. It consists of building pyramid layers for the raster and then plotting those instead. You'll still need the full `data` array that you already read in, however, because you need actual pixel data when sampling the raster.\n",
    "\n",
    "First you need to build pyramids for the aster image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.BuildOverviews('average', [2, 4, 8, 16, 32])\n",
    "ds.FlushCache()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use the `dstack()` method to read in the coarsest overview for each band and combine them (this is the method that was used for older versions of GDAL earlier in the notebook).\n",
    "\n",
    "We know there are five overview levels because we just built them. The last one is the coarsest, and it's at index 4."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ov_data = np.dstack([ds.GetRasterBand(i + 1).GetOverview(4).ReadAsArray() for i in range(ds.RasterCount)])\n",
    "ov_data.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that this new 3D array has considerably fewer rows and columns, so it'll plot much faster. It's useless for sampling pixel values, though."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(ov_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set an extent for `imshow()`\n",
    "\n",
    "Notice the coordinates in the above plot printed along the axes? They start at 0, but you know that the real-world coordinates of this raster don't start at 0. The `data` NumPy array just has pixel values and doesn't know anything about the real-world coordinates, so the plot uses pixel coordinates (row and column offsets). You can tell `imshow()` what the true coordinates are, however, by providing an extent. Let's see how to do that, just so you can draw the points from the shapefile on top of it later.\n",
    "\n",
    "Remember the geotransform, that has the upper left coordinates along with the cell size?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gt = ds.GetGeoTransform()\n",
    "gt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pull the information that you need out of this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the upper left x and y coordinates.\n",
    "ulx = gt[0]\n",
    "uly = gt[3]\n",
    "\n",
    "# Get the cell size\n",
    "cell_width = gt[1]\n",
    "cell_height = gt[5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use that information to calculate the lower right coordinates. For example, if you have the left-most coordinate (`ulx`), you can calculate the right-most coordinate (`lrx`) by multiplying the number of columns by the column width and then adding that to the left-most coordinate.\n",
    "\n",
    "So if the left-most coordinate is 20, the columns are 5 units wide, and there is only one column, then the right-most coordinate is 25. If there are two columns, the right-most coordinate is 30, and so on."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Problem 1\n",
    "\n",
    "Fill in the missing code to calculate the lower right y coordinate."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate the lower right coordinates.\n",
    "lrx = ulx + (cols * cell_width)\n",
    "lry = \n",
    "lrx, lry"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check your work:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "assert lry == 4586927.5, 'Nope'\n",
    "print('Looks good')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can provide an extent in the form of `[min_x, max_x, min_y, max_y]` when calling `imshow()`. Notice that the numbers on the axes now look like UTM coordinates (because they are!). The extent is the same whether we're plotting `data` or `ov_data`, so let's plot `ov_data` so things go faster. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(ov_data, extent=[ulx, lrx, lry, uly])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Use a shapefile\n",
    "\n",
    "Okay, now open the shapefile with GeoPandas:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the sites shapefile.\n",
    "sites = gpd.read_file('sites.shp')\n",
    "sites.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot vector data on top of raster\n",
    "\n",
    "Let's plot the points on top of the raster, since you went to all of the trouble to figure out how to tell `imshow()` the true raster coordinates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Capture the result of imshow so we can draw the points on the same plot.\n",
    "img = plt.imshow(ov_data, extent=[ulx, lrx, lry, uly])\n",
    "\n",
    "# Plot the points, and tell it to use the axis from the img variable returned from imshow.\n",
    "sites.plot(ax=img.axes, color='yellow')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Cool. Now you know that the points actually fall on top of the raster!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Convert a single set of x, y coordinates to pixel offsets\n",
    "\n",
    "In order to sample the raster at the coordinates shown in the shapefile, you need to be able to convert those coordinates to row and column offsets in the raster. You saw how to do this in the GDAL week, but you can refresh your memory in the [Raster Processing](../11-gdal/2-raster-processing.ipynb#Convert-real-world-coordinates-to-pixel-offsets) notebook.\n",
    "\n",
    "In order to do the conversion, you need the inverse geotransform, so let's get that:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the inverse geotransform, which will help convert real-world coordinates to pixel offsets.\n",
    "inverse_gt = gdal.InvGeoTransform(gt)\n",
    "inverse_gt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now see if you can sample the raster at the first point in the shapefile. You can use `at` to get the geometry from the row with index value 0:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "geom = sites.at[0, 'geometry']\n",
    "print(geom)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you have the geometry, you can use its x and y values, along with the inverse geotransform, to get the row and column offsets."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gdal.ApplyGeoTransform(inverse_gt, geom.x, geom.y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember that you need to use `floor` to convert the offsets to integers, so let's use a list comprehension to do that and store the results in `col` and `row` variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "col, row = [math.floor(n) for n in gdal.ApplyGeoTransform(inverse_gt, geom.x, geom.y)]\n",
    "col, row"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sample the raster at a single point\n",
    "\n",
    "Now use those offsets to sample the raster. It'll return the values of all three bands in a numpy array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data[row, col]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Function to sample the raster at x, y coordinates\n",
    "\n",
    "Okay, now you (hopefully!) have the process down. Let's write a function that'll do it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sample(geom, data, gt):\n",
    "    \"\"\"\n",
    "    geom: point geometry\n",
    "    data: numpy array to sample\n",
    "    gt: inverse geotransform that corresponds to the data array\n",
    "    \"\"\"\n",
    "    col, row = [math.floor(n) for n in gdal.ApplyGeoTransform(gt, geom.x, geom.y)]\n",
    "    return data[row, col]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Try it out with your geometry that you got earlier:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sample(geom, data, inverse_gt)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Those are the same results you got earlier, so it's working."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Apply the sample function to the entire dataset\n",
    "\n",
    "Now you're going to see a new Pandas function called `apply()` that allows you to apply a custom function to a dataframe. There are a few variations on how it works, but you're going to apply your `sample()` function to the geometry column only. So for each row in the dataframe, the geometry value will be fed into `sample()` as the first parameter. You can provide the `data` and `gt` parameters for `sample()` by name when you call `apply()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Apply the sample() function to the geometry column only.\n",
    "result = sites.geometry.apply(sample, data=data, gt=inverse_gt)\n",
    "result.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This gave you a pandas series containing the numpy arrays. They're not separated out into different columns, however, which is a slight problem. Let's convert this into another pandas dataframe that does have them separated out. You'll also specify what to call the new columns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result_df = pd.DataFrame.from_records(result, columns=['green', 'red', 'nir'])\n",
    "result_df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See the index values in the new `result_df` dataframe? They match the index values in the original `sites` dataframe, and because of this you can automagically combine the two dataframes together with the Pandas `join()` function. You'll add your sample results to the `sites` dataframe and store that result in a new dataframe called `sites2`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sites2 = sites.join(result_df)\n",
    "sites2.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At this point, you could use `to_file()` to save this as a new shapefile, or `to_csv()` to save the table to a csv file. Go ahead and make a new shapefile."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sites2.to_file('sites2.shp')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Everything together\n",
    "\n",
    "This might've seemed like a lot of code, but it's really not, once you take out the explanations and examples along the way. Here it is without the plotting and the code condensed a bit:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sample(geom, data, gt):\n",
    "    \"\"\"\n",
    "    geom: point geometry\n",
    "    data: numpy array to sample\n",
    "    gt: inverse geotransform that corresponds to the data array\n",
    "    \"\"\"\n",
    "    col, row = [math.floor(n) for n in gdal.ApplyGeoTransform(gt, geom.x, geom.y)]\n",
    "    return data[row, col]\n",
    "\n",
    "\n",
    "# Open aster.img, get the inverse geotransform, and read the data into an array.\n",
    "# Close the dataset because we don't actually need it anymore.\n",
    "ds = gdal.Open('aster.img')\n",
    "inverse_gt = gdal.InvGeoTransform(ds.GetGeoTransform())\n",
    "# data = ds.ReadAsArray(interleave='pixel')\n",
    "data = np.dstack([ds.GetRasterBand(i + 1).ReadAsArray() for i in range(ds.RasterCount)])\n",
    "del ds\n",
    "\n",
    "# Open the sites shapefile and sample the data.\n",
    "sites = gpd.read_file('sites.shp')\n",
    "result = sites.geometry.apply(sample, data=data, gt=inverse_gt)\n",
    "\n",
    "# Convert the result to a dataframe, join it to the original dataframe, and save.\n",
    "result_df = pd.DataFrame.from_records(result, columns=['green', 'red', 'nir'])\n",
    "sites2 = sites.join(result_df)\n",
    "sites2.to_file('sites2.shp')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The code could be condensed even more, but it would probably be a bit harder for you to follow because it would get rid of a lot of the intermediate variables. There are also faster ways to do this, but I figured this one would be easier for you to understand. And it's pretty fast as it is."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Use a CSV file\n",
    "\n",
    "What if your coordinates were in a csv file? Let's look at sites.csv."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.read_csv('sites.csv').head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Well, that's convenient, because the columns are called `x` and `y`. Why do I say it's convenient? Because you can get the x and y values from a row in the dataframe using the same syntax as you use to get the coordinates from a geometry. So even though you wrote your `sample` function to work with a geometry, it would actually work with this csv, too. You'd only have to change three lines in your code from before in order to make it work if you were happy with csv output.\n",
    "\n",
    "1. Use use `pd.read_csv()` instead of `gpd.read_file()` to open the file.\n",
    "2. Change the call to `apply()` so that it works on an entire row in the dataframe instead of just the geometry column.\n",
    "3. Use `to_csv()` instead of `to_file()` to save the result. You can't use `to_file()` to create a shapefile because you never actually create any geometries, and shapefiles need geometries.\n",
    "\n",
    "Here's the code. I left out the function and the code that reads the raster data, because you already have those in memory. The first two lines and the very last line of code are the only ones changed from earlier. The comments explain the new parameters for the changed lines."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the sites csv.\n",
    "sites = pd.read_csv('sites.csv')\n",
    "\n",
    "# Use apply() with the entire row. We have to use axis='columns' so that\n",
    "# it applies the sample() function to each row rather than each column (which\n",
    "# would be axis='index').\n",
    "result = sites.apply(sample, axis='columns', data=data, gt=inverse_gt)\n",
    "\n",
    "# Convert the result to a dataframe and join it to the original dataframe.\n",
    "result_df = pd.DataFrame.from_records(result, columns=['green', 'red', 'nir'])\n",
    "sites2 = sites.join(result_df)\n",
    "\n",
    "# Use index=False when writing the csv because we don't need the column with\n",
    "# 0, 1, 2, etc added to the output.\n",
    "sites2.to_csv('sites2.csv', index=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check out the result:   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.read_csv('sites2.csv').head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the coordinate columns weren't called `x` and `y`, then you'd have to change your `sample()` function to use the new names (you might also want to change the first parameter so it was called `record` instead of `geom`, since it's not really a geometry anymore). For example, `geom.x` might get changed to `record.longitude`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create a shapefile from CSV\n",
    "\n",
    "What if you wanted to create a shapefile instead of a csv? The biggest issue is that you'd need some point geometries, but there's a GeoPandas function for that! Watch this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sites = pd.read_csv('sites.csv')\n",
    "sites.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `points_from_xy()` will create a list of point geometries from the x and y columns, one for each row in the dataframe:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Tell points_from_xy where the x and y coordinates come from.\n",
    "points = gpd.points_from_xy(sites.x, sites.y)\n",
    "points"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you know how to create the geometries, you can use them to create a geodataframe from the regular dataframe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a new geodataframe using the data in sites and the geometries\n",
    "# we created a minute ago. Specify the coordinate system using EPSG code,\n",
    "# which happens to be 32612 for WGS 84 UTM zone 12 North.\n",
    "sites_geo = gpd.GeoDataFrame(sites, geometry=points, crs='epsg:32612')\n",
    "sites_geo.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You don't need the intermediate `points` variable and could instead do it like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sites_geo = gpd.GeoDataFrame(sites, geometry=gpd.points_from_xy(sites.x, sites.y), crs='epsg:32612')\n",
    "sites_geo.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You would obviously still have to save the dataframe to a shapefile. \n",
    "\n",
    "Let's look at the data types for the two dataframes, so you can see that one is a Pandas dataframe and the other is a GeoPandas geodataframe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(type(sites))\n",
    "print(type(sites_geo))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Homework\n",
    "\n",
    "If you wanted to sample the raster at the points in the csv, you could create the geodataframe first and then use it to sample the raster, or you could use the csv to sample the raster and then convert the resulting dataframe to a geodataframe. In fact, that's going to be one of your homework problems. "
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
