# GDAL & GeoPandas homework

**Due April 12 at midnight**

If you're having trouble finding a computer to use, try it online with this link: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/wild-6920%2Fweb-access/master)

## Notebook comprehension questions

*Worth 6 points*

- 1-sample-raster.ipynb (2 points)
- 2-zonal-stats.ipynb (3 points)
- 3-command-line-tools.ipynb (1 point)


## Script problems

*Worth 10 points each*

I did all of these problems and mostly just copy/pasted from the notebooks and changed the code to use the variables. The only problem that really required me adding my own code was problem 3. I'm telling you this so that if you can't do it with almost entirely copy/paste, you're probably making it too hard.

As always, make sure that your notebooks run if you restart the kernel and that you've used the variables from the first cell rather than typing out the filenames later on.

## Problem 1

Create a notebook called `problem1`. Set up four variables in the very first cell, with these names and values (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\WILD6920\Spring2020\python-gis\13-gdal-geopandas\data'
raster_filename = 'aster.img'
csv_filename = 'sites.csv'
shapefile_filename = 'sample_sites.shp'
```

Give this cell a `parameters` tag.

Your notebook needs to sample the file specified by `raster_filename` at the points specified by `csv_filename` and save the results to a new shapefile with `shapefile_filename`. Sample all three bands, and call them 'green', 'red', and 'nir'. Use an EPSG code of 32612 for the spatial reference. 

After you've created your shapefile, open it up again, count the number of rows with `len()`, display at least the first few rows, and also plot it on top of aster.img. You need to open it up again in order to make sure everything got saved correctly, and you need to plot it in order to make sure the spatial reference worked.

The first few lines of your geodataframe should look like this and it should have 42 rows in total:

![problem 1 results](images/problem1.png)


## Problem 2

Create a notebook called `problem2`. Set up three variables in the very first cell, with these names and values (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\WILD6920\Spring2020\python-gis\13-gdal-geopandas\data'
raster_filename = 'PRISM_ppt_30yr_normal_4kmM2_annual_bil.bil'
csv_filename = 'max_precip.csv'
```

Give this cell a `parameters` tag.

Find the maximum precipitation value for each state and save the results to a csv file. Put the state FIPS code in the first column of the csv, the state name in the second column, and the max precipitation value in the third.

*Hint: Check out the [binned_statistic](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.binned_statistic.html) documentation for help calculating the statistic.*

After you've created your output csv, open it up again, count the rows, and display at least the first few rows so you can make sure the columns are in the right order.

The first few lines of your dataframe should look like this and it should have 49 rows in total:

![problem 2 results](images/problem2.png)


## Problem 3

Make a copy your notebook from problem 2 and name it `problem3` (see the Make a Copy option under the File menu). Set up three variables in the very first cell, with these names and values (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\WILD6920\Spring2020\python-gis\13-gdal-geopandas\data'
raster_filename = 'PRISM_ppt_30yr_normal_4kmM2_annual_bil.bil'
csv_filename = 'precip_stats.csv'
```

Give this cell a `parameters` tag.

Modify your answer to problem 2 so that it calculates the min, max, mean, standard deviation, and range statistics. Write all of these to one csv file. Don't remove other things out of the notebook-- for example, keep the part at the end where it counts the rows and prints out part of the dataset.

*Hint 1: You can only calculate one statistic at a time, but you've seen how to combine the state abbreviations with one set of statistics. Just expand on this idea. There is no function for range, but it's just max - min, right? So it's easy to calculate once you have your dataframe.*

*Hint 2: Capitalize your column names like I did, otherwise you might run into conflicts with the built-in functions like min and max.*

The first few lines of your dataframe should look like this and it should have 49 rows in total:

![problem 3 results](images/problem3.png)
