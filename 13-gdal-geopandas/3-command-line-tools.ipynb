{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "**Don't change the data folder.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Don't change this.\n",
    "data_folder = 'data'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "from osgeo import gdal\n",
    "import geopandas as gpd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from IPython.display import Image\n",
    "\n",
    "# Change the Python working directory.\n",
    "os.chdir(data_folder)\n",
    "\n",
    "# Set up matplotlib to draw in the notebook.\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You'll need the GDAL_DATA environment variable to be set for this, so run this code to set it in case it isn't already set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not os.environ.get('GDAL_DATA'):\n",
    "    print('Updating GDAL_DATA')\n",
    "    os.environ['GDAL_DATA'] = os.path.join(os.environ['CONDA_PREFIX'], 'Library', 'share', 'gdal')\n",
    "else:\n",
    "    print('Nothing to do')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# GDAL command line utilities\n",
    "\n",
    "I'll admit that this doesn't actually have anything to do with Python. But it's useful information and people are usually pretty excited to learn about it. So here you go.\n",
    "\n",
    "GDAL comes with a whole bunch of [command line utilities](https://gdal.org/programs/index.html) that you don't use with Python. You could use them from the ArcGIS Python command prompt (or a regular Anaconda Prompt if you're using that instead of ArcGIS). You'll use them in the notebook by calling a *shell* command with `!`. Prefixing a line in a code cell with `!` means that it's code that would normally be run in a shell, and so the notebook sends the command to the operating system and then displays the results in the notebook. **This is not Python code.** It *does* use the Python working directory when looking for files, however, which is why you won't have to provide full paths. And the notebook will use Python code to display the data.\n",
    "\n",
    "I'm only going to show you a two or three of the utilities, and only part of what those ones can do. You can follow the link above to see the entire list."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## gdalinfo\n",
    "\n",
    "The [gdalinfo](https://gdal.org/programs/gdalinfo.html) utility will print out a whole ton of information about a raster dataset. Here are the meanings of the two options in the example:\n",
    "\n",
    "- `-norat`: Don't print the raster attribute table.\n",
    "- `-nomd`: Don't print the metadata.\n",
    "\n",
    "If you remove one or both of those options this will print out much more information. Go ahead and try it if you want, after seeing what it looks like this them in place.\n",
    "\n",
    "You'll see that this tells you size, extent, spatial reference, data type, and statistics for the raster. That's a lot handier than opening the raster in ArcGIS just to look that information up!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gdalinfo aster.img -norat -nomd"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## gdaladdo\n",
    "\n",
    "The [gdaladdo](https://gdal.org/programs/gdaladdo.html) tool will add overviews (pyramid layers) to a raster dataset. You probably already have overviews for the aster image because they were built in the first notebook, but let's rebuild them just in case.\n",
    "\n",
    "- `-r average`: Use averages for resampling.\n",
    "- `aster.img`: The file to build overviews for.\n",
    "- `2 4 8 16 32`: The levels to build."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gdaladdo -r average aster.img 2 4 8 16 32"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## gdalwarp\n",
    "\n",
    "You can use [gdalwarp](http://gdal.org/gdalwarp.html) to reproject and/or clip raster datasets. There are tons of options and ways you can combine them to get different results, but you'll only see a few examples here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Clip a raster\n",
    "\n",
    "This example will clip the aster image by the logan shapefile, but first take a look at the two files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Convenience function for plotting rasters in the notebook\n",
    "def show_image(fn):\n",
    "    ds = gdal.Open(fn)\n",
    "    \n",
    "    # If there are overviews\n",
    "    if ds.GetRasterBand(1).GetOverviewCount() > 0:\n",
    "        ov_index = ds.GetRasterBand(1).GetOverviewCount() - 1\n",
    "        for i in range(ds.RasterCount):\n",
    "            data = [ds.GetRasterBand(i + 1).GetOverview(4).ReadAsArray() for i in range(ds.RasterCount)]\n",
    "    # If there aren't overviews\n",
    "    else:\n",
    "        data = [ds.GetRasterBand(i + 1).ReadAsArray() for i in range(ds.RasterCount)]\n",
    "        \n",
    "    # If there was only one band, then just get the data out of the data list, otherwise use dstack.\n",
    "    # If we use dstack with only one band then things break.\n",
    "    if len(data) == 1:\n",
    "        data = data[0]\n",
    "    else:\n",
    "        data = np.dstack(data)\n",
    "\n",
    "    # Get the extent\n",
    "    ulx, cwidth, rotx, uly, roty, cheight = ds.GetGeoTransform()\n",
    "    lrx = ulx + (ds.RasterXSize * cwidth)\n",
    "    lry = uly + (ds.RasterYSize * cheight)\n",
    "    del ds\n",
    "    \n",
    "    # Plot the raster and return the image handle so something can be drawn on top if needed\n",
    "    return plt.imshow(data, extent=[ulx, lrx, lry, uly])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot logan on top of aster\n",
    "logan = gpd.read_file('logan.shp')\n",
    "img = show_image('aster.img')\n",
    "logan.plot(ax=img.axes, color='yellow')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You're going to clip the aster image to the yellow polygon. This is what the options mean:\n",
    "\n",
    "- `-cutline logan.shp`: logan.shp contains the layer to clip by.\n",
    "- `-cl logan`: Clip by the \"logan\" layer in logan.shp (because logan.shp was used with `-cutline`.\n",
    "- `-crop_to_cutline`: Make the output raster match the extent of the shapefile instead of the original raster.\n",
    "- `-overwrite`: Overwrite the output file if it already exists.\n",
    "- `aster.img`: The name of the raster to clip.\n",
    "- `clipped.tif`: The name of the raster to create."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gdalwarp -cutline logan.shp -cl logan -crop_to_cutline -overwrite aster.img clipped.tif"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Take a look at the clipped raster:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "show_image('clipped.tif')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use `gdalinfo` to make sure it still has three bands:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gdalinfo clipped.tif"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Reproject a raster\n",
    "\n",
    "This example reprojects the clipped raster to WGS84 lat/lon. The EPSG code for that is 4326 (this was called a *factory code* when we were talking about ArcGIS).\n",
    "\n",
    "- `-t_srs EPSG:4326`: The target SRS is EPSG 4326 (has to use all caps).\n",
    "- `-r near`: Use nearest neighbor resampling.\n",
    "- `clipped.tif`: The raster to reproject.\n",
    "- `clipped_geo.tif`: The raster to create."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gdalwarp -t_srs EPSG:4326 -r near clipped.tif clipped_geo.tif"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare the two rasters. They obviously use different coordinate systems."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(figsize=(10, 5))\n",
    "\n",
    "plt.subplot(1, 2, 1)\n",
    "show_image('clipped.tif')\n",
    "plt.title('clipped.tif')\n",
    "\n",
    "plt.subplot(1, 2, 2)\n",
    "show_image('clipped_geo.tif')\n",
    "plt.title('clipped_geo.tif')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Resample a raster\n",
    "\n",
    "This resamples aster from 15 meter pixels to 45 meter pixels.\n",
    "\n",
    "- `-tr 45 45`: The output pixels should be 45 map units (meters, in this case) wide and tall.\n",
    "- `-r bilinear`: Use bilinear resampling.\n",
    "- `-overwrite`: Overwrite the output file if it already exists.\n",
    "- `aster.img`: The name of the raster to resample.\n",
    "- `resampled.tif`: The name of the raster to create."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gdalwarp -tr 45 45 -r bilinear -overwrite aster.img resampled.tif "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This zooms in on a small area of the rasters (band 1 only) and compares the original with the resampled:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(10, 4))\n",
    "\n",
    "# Original\n",
    "plt.subplot(1, 2, 1)\n",
    "ds = gdal.Open('aster.img')\n",
    "plt.imshow(ds.GetRasterBand(1).ReadAsArray(3000, 3000, 120, 120), cmap='gray')\n",
    "plt.title('15 x 15')\n",
    "plt.axis('off')\n",
    "del ds\n",
    "\n",
    "# Resampled\n",
    "plt.subplot(1, 2, 2)\n",
    "ds = gdal.Open('resampled.tif')\n",
    "plt.imshow(ds.GetRasterBand(1).ReadAsArray(1000, 1000, 40, 40), cmap='gray')\n",
    "plt.title('45 x 45')\n",
    "plt.axis('off')\n",
    "del ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## gdal_translate\n",
    "\n",
    "You can use [gdal_translate](https://gdal.org/programs/gdal_translate.html) to convert rasters to different file types, but it can also resample. Let's compare its resample results with those of gdalwarp.\n",
    "\n",
    "### Resample a raster\n",
    "\n",
    "- `-tr 45 45`: The output pixels should be 45 map units (meters, in this case) wide and tall.\n",
    "- `-r bilinear`: Use bilinear resampling.\n",
    "- `aster.img`: The name of the raster to resample.\n",
    "- `resampled2.tif`: The name of the raster to create."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gdal_translate -tr 45 45 -r bilinear aster.img resampled2.tif "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now compare these results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(10, 4))\n",
    "\n",
    "# Original\n",
    "plt.subplot(1, 2, 1)\n",
    "ds = gdal.Open('aster.img')\n",
    "plt.imshow(ds.GetRasterBand(1).ReadAsArray(3000, 3000, 120, 120), cmap='gray')\n",
    "plt.title('15 x 15')\n",
    "plt.axis('off')\n",
    "del ds\n",
    "\n",
    "# Resampled\n",
    "plt.subplot(1, 2, 2)\n",
    "ds = gdal.Open('resampled2.tif')\n",
    "plt.imshow(ds.GetRasterBand(1).ReadAsArray(1000, 1000, 40, 40), cmap='gray')\n",
    "plt.title('45 x 45')\n",
    "plt.axis('off')\n",
    "del ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Convert raster file types\n",
    "\n",
    "You can also just convert a raster to a new file type without changing anything else. For example, this just converts aster.img into a GeoTiff.\n",
    "\n",
    "- `-of GTiff`: Output format is GeoTIFF (this isn't actually needed, because it's the default. It's why you didn't need this option for the previous examples that all created GeoTIFFs).\n",
    "- `aster.img`: The name of the raster to resample.\n",
    "- `aster.tif`: The name of the raster to create."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gdal_translate -of GTiff aster.img aster.tif"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or how about this? It resamples aster.img to 180 meter pixels and saves it as a PNG."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gdal_translate -of PNG -tr 180 180 -r bilinear aster.img aster.png"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PNG files can be opened with other software, so let's use IPython `Image()` to display it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Image('aster.png')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Change raster band order\n",
    "\n",
    "This next example uses the `-b` option to specify the orders of the bands in the output file. They are put into the output file in the order they're listed in the command, so if `-b 3` comes first, then band 3 becomes band 1 in the new file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gdal_translate -of PNG -tr 180 180 -r bilinear -b 3 -b 2 -b 1 aster.img aster2.png"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here's the result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Image('aster2.png')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## DEM tools\n",
    "\n",
    "The [gdaldem](https://gdal.org/programs/gdaldem.html) tool can be use to calculate slop, aspect, terrain ruggedness, and other things. Here's a slope example (you can probably figure out what's going on):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!gdaldem slope cach_ned10_clip_int.img slope.tif"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This shows the original DEM and the new slope raster side by side."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(12, 4))\n",
    "\n",
    "# DEM\n",
    "plt.subplot(1, 2, 1)\n",
    "show_image('cach_ned10_clip_int.img')\n",
    "plt.title('DEM')\n",
    "plt.axis('off')\n",
    "\n",
    "# Slope. The show_image function doesn't deal with NoData values,\n",
    "# but this doesn't draw correctly unless those are dealt with.\n",
    "plt.subplot(1, 2, 2)\n",
    "ds = gdal.Open('slope.tif')\n",
    "band = ds.GetRasterBand(1)\n",
    "data = np.ma.masked_equal(band.ReadAsArray(), band.GetNoDataValue())\n",
    "ulx, cwidth, rotx, uly, roty, cheight = ds.GetGeoTransform()\n",
    "lrx = ulx + (ds.RasterXSize * cwidth)\n",
    "lry = uly + (ds.RasterYSize * cheight)\n",
    "plt.imshow(data, extent=[ulx, lrx, lry, uly], norm=plt.Normalize(data.min(), data.max()))\n",
    "plt.title('Slope')\n",
    "plt.axis('off')\n",
    "del ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ogrinfo\n",
    "\n",
    "You can use [ogrinfo](https://gdal.org/programs/ogrinfo.html) to get information about a vector dataset, like a shapefile. This example gets info about the sites shapefile.\n",
    "\n",
    "- `-al`: Show info about all layers. This is designed to work with lots of different types of data, many of which can support multiple layers in a dataset (like a geodatabase, for example). Shapefiles can only have one layer. If you use this option, you don't need to know what the internal name of the shapefile's layer is.\n",
    "- `-so`: Summary only. Otherwise it will print out info about all of the features in the shapefile.\n",
    "\n",
    "You'll see that it tells you the the type of geometry, the number of features, the spatial reference, and the attribute field names and types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ogrinfo -al -so sites.shp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This shows info about all of the grass features.\n",
    "\n",
    "- `-where \"COVER='grass'\"`: Select out the features where the COVER field is equal to 'grass'. Notice that you didn't use the `-so` option this time, because you want to see info about the selected features.\n",
    "- `-al`: Show info about all layers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ogrinfo -where \"COVER='grass'\" -al sites.shp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ogr2ogr\n",
    "\n",
    "This is the last tool I'm going to show you. [ogr2ogr](https://gdal.org/programs/ogr2ogr.html) can be used to convert vector data between data types. Like gdal_translate, there are tons of options, but I'll just show you how to convert the sites shapefile to KML.\n",
    "\n",
    "- `-of kml`: Output format is kml.\n",
    "- `sites.kml`: Name for new file (notice that it comes before the name of the original file for this tool, for whatever reason).\n",
    "- `sites.shp`: Name of original file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ogr2ogr -of kml sites.kml sites.shp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now if you have Google Earth, try opening up sites.kml. If you click on one of the site pins, you'll see that it kept the attribute data."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
