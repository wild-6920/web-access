{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "**Don't change the data folder.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Don't change this.\n",
    "data_folder = 'data'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "from osgeo import gdal\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import geopandas as gpd\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Change the Python working directory.\n",
    "os.chdir(data_folder)\n",
    "\n",
    "# Set up matplotlib to draw in the notebook.\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Demo: Zonal statistics\n",
    "\n",
    "This example will walk you through the process of calculating the mean precipitation for each state when the precipitation data is a raster and the states are vector. If you were to run [Zonal Statistics](https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/zonal-statistics.htm) in ArcGIS, it would convert the counties to a raster in memory in order to do the calculations-- it just doesn't tell you that it's doing that. You'll need to do it yourself in this example (you don't expect free software to make life quite as easy as incredibly expensive software, do you?).\n",
    "\n",
    "If you're not familiar with zonal statistics, you might want to read Esri's documentation for the [tool](https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/zonal-statistics.htm) and a more [involved description](https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/how-zonal-statistics-works.htm), so you know what we're trying to accomplish here.\n",
    "\n",
    "So here's what you're going to do:\n",
    "\n",
    "1. Create a new raster with the same cell size and extent as the precipitation one\n",
    "2. Rasterize the states\n",
    "3. Calculate the statistics\n",
    "4. Write the statistics to a text file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create a new raster with same cell size and extent as the precipitation raster\n",
    "\n",
    "The precipitation data is a PRISM dataset from http://prism.oregonstate.edu/normals/. It's in BIL format, which GDAL will read without a problem. You need to open it in order to get the required information about it, and you might as well read in the data while you're at it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the raster dataset.\n",
    "prism_ds = gdal.Open('PRISM_ppt_30yr_normal_4kmM2_annual_bil.bil')\n",
    "if not prism_ds:\n",
    "    raise IOError(\"Couldn't load PRISM data\")\n",
    "    \n",
    "# Get the band.\n",
    "prism_band = prism_ds.GetRasterBand(1)\n",
    "\n",
    "# Read all of the data into memory.\n",
    "prism_data = prism_band.ReadAsArray()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's see what the dataset looks like. Plot it in green."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(prism_data, cmap='Greens')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You need to mask out the NoData areas if you don't want them included in the statistics you're going to calculate. That's because although GDAL knows what the NoData value is, NumPy doesn't and it will treat it like any other pixel value.\n",
    "\n",
    "What is the NoData value for this dataset, anyway?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nodata = prism_band.GetNoDataValue()\n",
    "nodata"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Okay, so let's mask out all pixels in `prism_data` where the cell value is equal to the NoData value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prism_data = np.ma.masked_equal(prism_data, prism_band.GetNoDataValue())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the data should plot differently. That's because matplotlib tries to scale the data between the min and max values when it plots it, and now the minimum value will be close to 0 (no precipitation) instead of -9999 (the NoData value)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(prism_data, cmap='Greens')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Rasterize the states\n",
    "\n",
    "Now you need a states raster that overlays perfectly on the precipitation raster. This way you can use the values in the states raster to group the cells in the precipitation raster by state so that you can calculate statistics for each state.\n",
    "\n",
    "You're going to use the [gdal.Rasterize()](../osgeo_docs/osgeo.gdal.html#-Rasterize) function for this. There are a few ways to use it, but in order to ensure that the cell size and extent match perfectly between the two rasters, you'll create an empty raster that `Rasterize()` can put the data into.\n",
    "\n",
    "You *could* save this raster for later, but instead you're going to create it in memory and it'll disappear when you're done with it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "### Problem 1\n",
    "\n",
    "This code creates a new raster in memory (notice the driver is called `MEM`) to hold the rasterized polygons. Replace the ## marks in the next bit of code so that it successfully creates a raster that matches the cell size and extent of the precipitation dataset. **Only use variables-- no numbers!** *You saw how to do this in the [raster-basics](../11-gdal/1-raster-basics.ipynb) notebook.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": [
    "states_ds = gdal.GetDriverByName('MEM').Create('states.tif', ##, ##)\n",
    "states_ds.SetProjection(##)\n",
    "states_ds.SetGeoTransform(##)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "Check your work."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": [
    "assert states_ds.RasterXSize == 1405 and states_ds.RasterYSize == 621, 'wrong numbers of rows and columns'\n",
    "assert states_ds.GetProjection() == prism_ds.GetProjection(), 'wrong spatial reference'\n",
    "assert states_ds.GetGeoTransform() == prism_ds.GetGeoTransform(), 'wrong geotransform'\n",
    "print('Looks good')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " The new band is automatically initialized to all zeros, and that's also what you'll use as the NoData value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the new states band and set its NoData value to 0.\n",
    "states_band = states_ds.GetRasterBand(1)\n",
    "states_band.SetNoDataValue(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You already read the precipitation data into memory, you've gotten all of the metadata from it that you need, so go ahead and close the file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "del prism_ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The [gdal.Rasterize()](../osgeo_docs/osgeo.gdal.html#-Rasterize) function will let you assign pixel values based on an attribute field in the vector layer, but the field must be numeric because raster pixel values can only be numbers. You'll use the STATE_FIPS field, which is an identifier unique to each state.\n",
    "\n",
    "This next bit of code tells GDAL to rasterize 'states.shp', put the rasterized data into our empty `states_ds`, and use the value of the STATE_FIPS field for the pixel values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gdal.Rasterize(states_ds, 'states.shp', attribute='STATE_FIPS')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your states raster data isn't in a NumPy array at the moment, so read it in, mask out the zeros (since they're NoData), and draw it. You'll also use this `states_data` array later to calculate the statistics."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read in the data from the states band and mask out the pixels that are 0.\n",
    "states_data = np.ma.masked_equal(states_band.ReadAsArray(), 0)\n",
    "\n",
    "# Plot it.\n",
    "plt.imshow(states_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Beautiful! "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculate the statistics\n",
    "\n",
    "Now it's time to set things up for the statistics calculation. You're going to use SciPy's `binned_statistic()` function. One of the optional parameters for this is a list of bins to use when grouping data for the calculation. You're going to group by the values in `states_data`, but you need to tell it what these groups are. Remember that `states_data` contains unique FIPS codes for each state, so you want each unique value of this array to be a group, or bin. The NumPy `unique` function will give you a sorted list of unique values in an array, which is a good starting point."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "states_bins = np.unique(states_data)\n",
    "states_bins"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There's a problem with this list of bins, though. It's a masked array, since the unique values were pulled from another masked array. Conveniently, the masked value is at the end, and you need to add another value onto the end anyway. This is because the bins you provide to `binned_statistic` need to be a list of min *and* max values for each bin. The first bin will include everything greater than or equal to 1 but less than 4, the second bin will include anything greater than or equal to 4 but less than 5, and so on. But you still need to provide an upper bound for the last bin. This is easy enough by filling the masked value with the maximum value of the array plus one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "states_bins = states_bins.filled(states_bins.max() + 1)\n",
    "states_bins"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Perfect. There's one more step required before you can calculate stats, though. The `binned_statistics()` function wants the input arrays to be one-dimensional. You can turn your 2D arrays into 1D with the array's `flatten()` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prism_data = prism_data.flatten()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "### Problem 2\n",
    "\n",
    "You just converted the `prism_data` array into a 1D array. Now do the same for `states_data`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "Check it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": [
    "assert states_data.shape == (872505,)\n",
    "print('Looks good')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you're all set. Here's the signature for [binned_statistics](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.binned_statistic.html):\n",
    "\n",
    "```python\n",
    "scipy.stats.binned_statistic(x, values, statistic='mean', bins=10, range=None)\n",
    "```\n",
    "\n",
    "The `x` parameter is the data that the bin boundaries will be applied to (i.e. the zones; this is your states dataset) and `values` is the data to calculate statistics on (precipitation). You want the mean, so the default for the `statistic` parameter is fine. The default for the `bins` parameter is 10, meaning SciPy would calculate the boundaries for 10 bins evenly distributed in the `x` data. You want to provide your own list of bin boundaries, however.\n",
    "\n",
    "The function returns an array of statistics, an array containing bin edges (useful if you let SciPy calculate the bins for you), and an array that indicates which bin each cell in the `values` array fell into. You're only interested in the statistics array, which contains the mean precipitation for each state."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scipy.stats\n",
    "\n",
    "# Calculate the zonal statistics on the prism data, using the states as zones.\n",
    "prism_stats, bin_edges, value_bins  = scipy.stats.binned_statistic(states_data, prism_data, bins=states_bins)\n",
    "\n",
    "# Print the statistics.\n",
    "print(prism_stats)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create the csv file\n",
    "\n",
    "Your end goal is to create a csv file with the state FIPS code, name, and mean precipitation. Although you can combine all of the various NumPy arrays into one and write that to a csv, I think it's easier to use Pandas, so that's what we'll do.\n",
    "\n",
    "Since the `states_bins` variable contains FIPS codes that match up with the mean precipitation values, you can create a Pandas dataframe with those two columns like this (notice that we're cutting off that last extra bin with `states_bins[:-1]`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Specify two columns and the data that will be in them.\n",
    "data = {'FIPS': states_bins[:-1], \n",
    "        'MeanPrecip': prism_stats}\n",
    "\n",
    "# Create a dataframe from the data just specified.\n",
    "df = pd.DataFrame(data)\n",
    "df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You also want the state names, which you can get from the states shapefile, so open it up with GeoPandas."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "states_df = gpd.read_file('states.shp')\n",
    "states_df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get rid of the columns you're not interested in."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "states_df = states_df[['STATE_FIPS', 'STATE_NAME']]\n",
    "states_df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you have one more problem- you can't merge on attributes that are different data types. For example, the number 5 will never match the string '5'. You can use `dtypes` to look at the data types for the columns in the two dataframes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.dtypes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "states_df.dtypes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The FIPS column in your means dataframe is unsigned 8-bit integer, but the STATE_FIPS column from the shapefile is an object (meaning it's a string). Go ahead and convert it to an integer."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "states_df.STATE_FIPS = states_df.STATE_FIPS.astype(int)\n",
    "states_df.dtypes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The two columns are both integers now. It doesn't matter that they're different *types* of integers-- they'll still match.\n",
    "\n",
    "Now use the FIPS columns to merge the state names column with `df`. Use `how='left'` to keep all of the rows from the left-hand dataframe (`df`) and not add any new ones that are in the shapefile (like Alaska or Hawaii). The `left_on` and `right_on` parameters specify the fields to use when merging."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = df.merge(states_df, how='left', left_on='FIPS', right_on='STATE_FIPS')\n",
    "df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now write this out to a csv, and reorder the columns while you're at it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.to_csv('precip.csv', columns=['FIPS', 'STATE_NAME', 'MeanPrecip'], index=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here's the output file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!more precip.csv"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
