import csv
from osgeo import gdal, ogr, osr

def problem1(fn):
    try:
        ds = gdal.Open(fn)
        assert ds is not None, 'Could not open ' + fn
        assert ds.RasterCount == 1, f'There are {ds.RasterCount} bands, but there should only be 1'
        assert ds.RasterXSize == 5665 and ds.RasterYSize == 5033, \
            f'The raster is {ds.RasterXSize} x {ds.RasterYSize}, but should be 5665 x 5033'
        srs = osr.SpatialReference(ds.GetProjection())
        srs.AutoIdentifyEPSG()
        assert srs.GetAttrValue('AUTHORITY', 1) == '32612', 'The spatial reference looks wrong'
        assert ds.GetGeoTransform() == (419976.5, 15.0, 0.0, 4662422.5, 0.0, -15.0), \
            'The geotransform looks wrong'
        band = ds.GetRasterBand(1)
        assert band is not None, 'Could not get band'
        assert band.DataType == gdal.GDT_Float32, 'Data type is not 32-bit floating point'
        assert band.GetNoDataValue() == -99, 'The NoData value is not -99'
        assert band.GetOverviewCount() == 5, "There aren't 5 pyramid levels"
        assert band.Checksum() in [12056, 25959], 'Pixel values look wrong'
        print('Problem 1: All tests passed.')
    finally:
        del ds

def problem2(fn):
    try:
        ds = gdal.Open(fn)
        assert ds is not None, 'Could not open ' + fn
        assert ds.RasterCount == 3, \
            f'There are {ds.RasterCount} bands, but there should be 3'
        assert ds.RasterXSize == 1719 and ds.RasterYSize == 1313, \
            f'The raster is {ds.RasterXSize} x {ds.RasterYSize}, but should be 1719 x 1313'
        srs = osr.SpatialReference(ds.GetProjection())
        srs.AutoIdentifyEPSG()
        assert srs.GetAttrValue('AUTHORITY', 1) == '32612', 'The spatial reference looks wrong'
        assert ds.GetGeoTransform() == (460026.5, 15.0, 0.0, 4644347.5, 0.0, -15.0), \
            'The geotransform looks wrong'
        for band, check in [(1, 1684), (2, 9839), (3, 49859)]:
            band = ds.GetRasterBand(band)
            assert band.DataType == gdal.GDT_Byte, f'Data type for band {band} is not Byte'
            assert band.Checksum() == check, f'The pixel values for band {band} look wrong.'
        print('Problem 2: All tests passed.')
    finally:
        del ds
