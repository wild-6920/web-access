{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# GDAL\n",
    "\n",
    "We're going to use free software instead of Esri this week. [GDAL](http://www.gdal.org/) is an extremely popular open source library for working with spatial data. It isn't written in Python, but there are Python bindings for it. The Python bindings are what allow you to use GDAL from Python, even though GDAL isn't Python software (just like ArcGIS isn't Python software, but you can use arcpy in Python). This means that you need to install both GDAL and the Python bindings in order for things to work, which really isn't any different from ArcPy, which requires that you have ArcGIS installed. If you installed software on your own computer, the setup scripts that I gave you also installed GDAL.\n",
    "\n",
    "There are three parts to the GDAL library:\n",
    "\n",
    "1. GDAL, for working with raster data\n",
    "2. OGR, for working with vector data\n",
    "3. OSR, for working with spatial references\n",
    "\n",
    "The GDAL part of it, especially, is used in lots of software packages. Even ArcGIS uses it.\n",
    "\n",
    "Like Python, GDAL is free software, and you can actually accomplish quite a lot with it, with very little work. This means that if you go to work for a small company that can't afford an Esri license, you can still do plenty of spatial analysis. You can also do small projects on your own without having to fork over thousands of dollars for software (or illegally using a license from your workplace or university for personal gain).\n",
    "\n",
    "## Documentation\n",
    "\n",
    "There isn't a website as extensive as Esri's, but the online documentation is at [gdal.org/python/](https://gdal.org/python/). This won't hold your hand nearly as much as the Esri documentation does-- actually, it doesn't at all!\n",
    "\n",
    "I also used [pydoc](https://docs.python.org/2/library/pydoc.html) to generate some *really ugly* documentation that's in your `osgeo_docs` folder, because it was easier to create links to from inside the notebook. This documentation is brief, however, and lots of times won't give you any more info than the notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "**Don't change the data folder.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_folder = 'data'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unlike arcpy, which has its own concept of a working directory (`arcpy.env.workspace`), GDAL uses the Python working directory, so you need to set it with `os.chdir()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "# Change the Python working directory.\n",
    "os.chdir(data_folder)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's set the notebook up for plotting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Set up matplotlib to draw in the notebook.\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Reading raster data with GDAL\n",
    "\n",
    "Originally GDAL was designed for reading and writing raster data, but it has gained some analysis capabilities over the years. You'll mostly use GDAL for file input and output, and NumPy and SciPy for working with the data.\n",
    "\n",
    "There are over 150 different [format drivers](https://www.gdal.org/formats_list.html) available for GDAL, although your version most certainly doesn't include all of them. You'll see how to find out which ones you have in a minute.\n",
    "\n",
    "You can import gdal from the osgeo package like the following cell. We're also going to import numpy and rename it to np, because we'll be using it later on:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from osgeo import gdal\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's see how to determine the available formats for your version of GDAL. \n",
    "\n",
    "Each file format is accessed using a particular driver. A driver can only work with one format type, although there are multiple available drivers for a few of the formats. For example, you need a different driver to work with GeoTIFFs and Imagine .img files. \n",
    "\n",
    "You can use the `GetDriverCount()` function to help loop through all of the drivers and get their names:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the number of available drivers and use it to set up the loop.\n",
    "for i in range(gdal.GetDriverCount()):\n",
    "    \n",
    "    # Get the driver at index i and print out its name.\n",
    "    print(gdal.GetDriver(i).ShortName)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can get information about each of these at http://www.gdal.org/formats_list.html.\n",
    "\n",
    "You can read data without specifying which driver to use and GDAL will figure it out, but if you want to create a new file you need to specify the driver so that the correct kind of file is created. You'll see this in action later, but I wanted to start off by showing how many different formats GDAL could work with."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Datasets and bands\n",
    "\n",
    "Raster datasets are just two or three-dimensional arrays of data, just like a photograph (in fact, a photo is a type of raster). The rows and columns of pixels in a photo make up a two-dimensional array. Depending on the format of your photo, there might be one or three *bands*, each of which is also a two-dimensional array. All bands have the same number of rows and columns, and together, they make up a three-dimensional array.\n",
    "\n",
    "Before you can work with raster data, you need to open the dataset and then get the band or bands that you want to work with."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Open a dataset\n",
    "\n",
    "You can use the `gdal.Open()` function to open a raster dataset. Just pass it the path to the file you want to open (you've already used `os.chdir()` to set the working directory to the folder that `aster.img` lives in, so there's no need to provide the full path)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open aster.img and put the dataset in the ds variable.\n",
    "ds = gdal.Open('aster.img')\n",
    "\n",
    "# Check to make sure that the dataset was opened successfully.\n",
    "if ds is None:\n",
    "    raise IOError('Could not open raster')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the dataset can't be opened, `Open()` returns `None`, so you can check for this before continuing. The `raise IOError()` stuff throws an exception so your script stops.\n",
    "\n",
    "Now that you have the raster dataset, you can get information about the number of rows (`RasterYSize`), columns (`RasterXSize`), and bands (`RasterCount`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get some info about the dataset.\n",
    "print('rows:', ds.RasterYSize)\n",
    "print('columns:', ds.RasterXSize)\n",
    "print('bands:', ds.RasterCount)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that although you can get information about the raster dataset, none of the pixel values have been read yet."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Get a band\n",
    "\n",
    "You use `GetRasterBand(index)` to get a band. **Band indices start at 1** instead of 0, so this gets the first band:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the first band from the dataset (doesn't read pixel data).\n",
    "band = ds.GetRasterBand(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The band also knows how many rows and columns it has:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the numbers of rows and columns in the band.\n",
    "print('rows:', band.YSize)\n",
    "print('columns:', band.XSize)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It also knows things like its min and max values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get min and max pixel values for the band.\n",
    "print('min:', band.GetMinimum())\n",
    "print('max:', band.GetMaximum())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, no pixel values have actually been read yet."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Read data into a NumPy array\n",
    "\n",
    "[NumPy](http://www.numpy.org/) is a widely used Python module for working with large arrays of data, and the easiest way to work with raster data in Python is to put it into a NumPy array. A lot of Python modules use NumPy to hold their data-- Pandas is one example of that. Because we'll be using NumPy to process the raster data once it's in memory, you'll see some of the basics of NumPy in the raster notebooks, but we'll only be scratching the surface.\n",
    "\n",
    "The raster band that you got a minute ago actually knows a lot more about itself than what you've already seen, and we'll come back to some of it later. For now, let's read in some data. So far all you really have is metadata-- data about the data. You don't have any actual pixel values until you read them in. Reading in an entire band into a NumPy array is easy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read all of the pixel data from the band into a numpy array called data.\n",
    "data = band.ReadAsArray()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the `data` variable holds a NumPy array. Here's how to get the array's statistics:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get some statistics about the numpy array. This is NumPy, not GDAL.\n",
    "print('sum:', data.sum())\n",
    "print('mean:', data.mean())\n",
    "print('std:', data.std())\n",
    "print('count:', len(data))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also pull out individual pixel values by providing offsets in **row, column order (that's y, x)** (mathematicians use row, column order, so that's why this is opposite of what you're used to using with GIS). \n",
    "\n",
    "This is the pixel at row 2500 and column 1400:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the pixel value at row 2500 and column 1400.\n",
    "data[2500, 1400]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "### Problem 1\n",
    "\n",
    "Get the pixel value at row 3500 and column 4000."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use *slices* to pull out multiple pixels at once. This example starts reading at row 2500 and reads up to (but not including) row 2503. As for columns it starts reading at column 1400 and reads up to (but doesn't include) column 1405. This results in a 5x5 array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get a 2D array of pixels, made of up rows 2500-2502 and columns 1400-1404\n",
    "data[2500:2503, 1400:1405]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "### Problem 2\n",
    "\n",
    "Get a 2D array of pixels, made up of rows 3500-3509 and columns 4000-4002 (that's 10 rows and 3 columns)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Raster algebra\n",
    "\n",
    "Once you read data in, you can treat the arrays like regular variables and do math with them. You'll try this out by calculating a [NDVI](https://en.wikipedia.org/wiki/Normalized_Difference_Vegetation_Index). You saw this in the Esri part of the class, too. Remember the equation?\n",
    "\n",
    "\\begin{align}\n",
    "NDVI = \\frac{NIR - RED}{NIR + RED}\n",
    "\\end{align}\n",
    "\n",
    "The third band in the ASTER image is near-infrared (NIR) and the second band is red, so let's read in the data for these two bands. \n",
    "\n",
    "Because the raster stores the data as unsigned 8-bit integers, that's how it's read in to numpy. That poses a problem, though, because we want to do floating point math that might have negative numbers, which isn't supported by that data type. So we need to convert at least one of the arrays to a different data type. Otherwise numpy does the math as unsigned 8-bit, which gives you the wrong result. To see what I mean, check out this example. It creates two sample arrays, each with two numbers in them, and then applies the NDVI equation to them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "red1 = np.array([33, 2]).astype('uint8')\n",
    "nir1 = np.array([61, 0]).astype('uint8')\n",
    "print((nir1 - red1) / (nir1 + red1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But you'd expect this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print((61 - 33) / (61 + 33))\n",
    "print((0 - 2) / (0 + 2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first answer (0.2978...) is correct, but the other one is wrong because the unsigned data type can't handle the negative number. See the `3-data-types` notebook for info on why this happens.\n",
    "\n",
    "But change one of the data types to something with more bits, and you get the correct answer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "red1 = np.array([33, 2]).astype('uint8')\n",
    "nir1 = np.array([61, 0]).astype('int32')\n",
    "print((nir1 - red1) / (nir1 + red1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So lets convert the NIR to 32-bit integer when we read it in."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read in the second (red) band and leave it as integer.\n",
    "red = ds.GetRasterBand(2).ReadAsArray()\n",
    "\n",
    "# Read in the third (near-infrared) band and convert it to floating point.\n",
    "nir = ds.GetRasterBand(3).ReadAsArray().astype('int32')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can check the data types to see that RED was automatically read in as unsigned 8-bit, but NIR was changed to 32-bit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('red:', red.dtype)\n",
    "print('nir:', nir.dtype)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's see what happens when you try some math."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ndvi = (nir - red) / (nir + red)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Use masked bands to handle division by zero errors\n",
    "\n",
    "Assuming you haven't changed your default Python settings, you should've seen an error that said `RuntimeWarning: divide by zero encountered in true_divide` when you ran that last bit of code. This is a division by zero error in the locations where nir + red == 0. The next couple of cells print out a small section of the arrays that shows this problem in the upper left. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This just changes print settings so that the arrays print out nicely aligned.\n",
    "# It has nothing to do with the analysis and you wouldn't normally do it.\n",
    "np.set_printoptions(formatter={'all': lambda x: '%06s' % x})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's the red band:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print out part of the red band.\n",
    "print(red[255:265, 1190:1195])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here's the near infrared:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print out part of the nir band.\n",
    "print(nir[255:265, 1190:1195])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Any locations that have a zero in both bands will result in that division by zero error. Depending on your settings, this may or may not be a problem. To be safe, you can mask out the areas that have a zero denominator (where nir + red = 0) so that numpy doesn't try to do any calculations on those cells.\n",
    "\n",
    "To do this, you'll use [np.ma.masked_where()](https://docs.scipy.org/doc/numpy-1.13.0/reference/generated/numpy.ma.masked_where.html) to provide a masking condition. The first parameter is the condition, and the second is the array to mask. You'll mask the red array and save it back into the same variable (`red`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Mask out pixels in the red band where nir + red == 0.\n",
    "red = np.ma.masked_where(nir + red == 0, red)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now if you print out part of the red band, you'll see that the locations that would result in an error have been masked out and no longer contain values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(red[255:265, 1190:1195])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you've masked the array, you can do your math without the division by zero errors. You could mask both arrays before doing the math, but it's really only necessary to do it on one of them because if one array in the equation is masked, then that pixel is ignored for the equation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ndvi = (nir - red) / (nir + red)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The result is still masked:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Again, this is just making it so the next print statement makes nicer output.\n",
    "np.set_printoptions(formatter={'all': lambda x: '%020s' % x}, linewidth=110)\n",
    "\n",
    "# Print part of the ndvi.\n",
    "print(ndvi[255:265, 1190:1195])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You're not quite done yet, though, because you need to take care of those masked pixels and replace them with a NoData value. Esri uses some huge number, but let's just use -99 because that's not a valid NDVI value and it's easy to remember. You can use the `filled()` method to fill all of the empty pixels with -99 and then print out that range of pixels again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fill the masked pixels with -99.\n",
    "ndvi = ndvi.filled(-99)\n",
    "\n",
    "# Print part of the ndvi.\n",
    "print(ndvi[255:265, 1190:1195])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember from the Esri section of the class that you can use matplotlib and `imshow()` to plot a numpy array. The `Normalize()` bit tells it to scale the values between -1 and 1 into values that it can plot, and the `cmap` is telling it to use grayscale."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(ndvi, norm=plt.Normalize(-1, 1), cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "### Problem 3\n",
    "\n",
    "We're done with the nir band, so now you're going to mask it for practice. Mask out all cells where the pixel value is greater than 150 and save the result back into the `nir` variable. See https://docs.scipy.org/doc/numpy/reference/generated/numpy.ma.masked_greater.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now print out the same pixels you were looking at earlier. All values greater than 150 should be missing, but the zeros should be back."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": [
    "print(nir[255:265, 1190:1195])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create a new dataset\n",
    "\n",
    "If you've gone to the trouble to calculate an NDVI, you probably want to write the data out to a file, which means you need to create one. First, you need to decide on a format and then get the appropriate driver using the code from http://www.gdal.org/formats_list.html. I like using the GeoTIFF driver because anything can read those. Once you have the driver, you can use its [`Create()`](../osgeo_docs/osgeo.gdal.html#Driver-Create) method to create a dataset:\n",
    "\n",
    "```\n",
    "<driver>.Create(filename, columns, rows, bands=1, data_type=GDT_Byte, options=None)\n",
    "```\n",
    "\n",
    "If you don't provide the optional parameters, the dataset will have one band with a data type of Byte (you can find the available data types at the bottom of [osgeo.gdal.html](../osgeo_docs/osgeo.gdal.html) (in your `osgeo_docs` folder)-- they're the ones that start with GDT).\n",
    "\n",
    "**This will overwrite existing rasters without asking (unless they're open somewhere, like ArcGIS, which keeps them from being deleted).**\n",
    "\n",
    "Let's create a dataset to hold the NDVI values. Remember when I said you had to specify the file format driver when creating a new dataset? This is where that happens."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the GeoTIFF driver.\n",
    "gtiff_driver = gdal.GetDriverByName('GTiff')\n",
    "\n",
    "# Create a new GeoTIFF with the same number of rows and columns as aster.img (ds), \n",
    "# one band, and a 32-bit floating point data type.\n",
    "ndvi_ds = gtiff_driver.Create('ndvi.tif', ds.RasterXSize, ds.RasterYSize, 1, gdal.GDT_Float32)\n",
    "if ndvi_ds is None:\n",
    "    raise IOError('Could not create raster')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that you used the number of rows and columns from the original ASTER dataset. And since NDVI values range between -1.0 and 1.0, you needed a floating point data type.\n",
    "\n",
    "While you're creating the dataset, you might as well copy the projection and georeferencing info from the original dataset, too. **This doesn't get done automatically.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Copy the projection info from aster.img into the ndvi raster.\n",
    "ndvi_ds.SetProjection(ds.GetProjection())\n",
    "\n",
    "# Copy the georeferencing info from aster.img into the ndvi raster.\n",
    "ndvi_ds.SetGeoTransform(ds.GetGeoTransform())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you're interested in what you were copying, the projection is just a spatial reference string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds.GetProjection()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And the geotransform is a tuple of 6 numbers that contains information about the upper left corner coordinates and pixel sizes:\n",
    "\n",
    "```\n",
    "(upper_left_x, pixel_width, x_rotation, upper_left_y, y_rotation, pixel_height)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ndvi_ds.GetGeoTransform()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The rotation values are 0 for a North-up image, which will be the case for most of your data. The pixel height is negative because it's measuring from the upper left corner and going down. This info, along with the number of rows and columns, is everything that software needs in order to draw the raster in the correct place on a map. This is a lot different than vector data, which needs coordinates for every vertex!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Write pixel values\n",
    "\n",
    "So now you now have a dataset with one band, but there isn't any actual data in it yet. If you're writing an entire band, all you need to do is pass the data array to the band's [`WriteArray`](../osgeo_docs/osgeo.gdal.html#Band-WriteArray) method, but you have to get the band first:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the band in the ndvi raster.\n",
    "ndvi_band = ndvi_ds.GetRasterBand(1)\n",
    "\n",
    "# Write the ndvi array into the ndvi band.\n",
    "ndvi_band.WriteArray(ndvi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It should've printed out a 0. That means it worked. Any other number would be an error code of some sort.\n",
    "\n",
    "Remember that you used -99 for a NoData value? You need to tell the band that that's what the -99's are, or else it will think they're real data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Tell it that -99 means NoData.\n",
    "ndvi_band.SetNoDataValue(-99)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compute statistics\n",
    "\n",
    "You'll probably want to calculate statistics on your band. You need to flush the data to disk before you can calculate stats, though. Passing `True` to `ComputeStatistics()` will allow it to use approximate statistics, and `False` will force it to compute actual statistics."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write data to disk.\n",
    "ndvi_band.FlushCache()\n",
    "\n",
    "# Calculate statistics, False means it's not allowed to estimate.\n",
    "stats = ndvi_band.ComputeStatistics(False)\n",
    "\n",
    "# Print the statistics that it calculated.\n",
    "stats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`ComputeStatistics` returns the min, max, mean, and standard deviation, in that order.\n",
    "\n",
    "Sometimes ArcMap won't display a raster correctly if statistics haven't been calculated, especially if it's an integer raster. I don't know about ArcGIS Pro."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Build pyramids\n",
    "\n",
    "You might also want to build pyramid, or overview, layers. You need to specify which levels you want it to build. A level of 2 will build a pyramid layer that is half the size as the original, a level of 4 will build one with 1/4 the number of rows and columns, and so on.\n",
    "\n",
    "A good rule of thumb is to create enough levels (each half the size as the previous) so that the coarsest ones are 256 pixels or less. So to determine an appropriate level, you can divide the larger of the number of rows and columns by 256, which will give an appropriate level-- sort of. The levels are from the series [2, 4, 8, 16, 32, 64, 128, ...], so use the smallest one of those that's bigger than the number you got from your division, and then include all the lower levels as well.\n",
    "\n",
    "For example, divide the numbers of rows and columns by 256:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(ndvi_ds.RasterXSize / 256)\n",
    "print(ndvi_ds.RasterYSize / 256)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The larger of these two numbers is 22, so use the smallest number in the series [2, 4, 8, 16, 32, 64, 128, ...] that's greater than or equal to 22. That's 32, so the appropriate pyramid levels to build are all the ones up to 32 (`[2, 4, 8, 16, 32]`).\n",
    "\n",
    "The possible resampling types for building pyramids are nearest, average, gauss, cubic, cubicspline, lanczos, average_mp, average_magphase, and mode. The default is nearest, which is what you'd **always** want to use for categorical data, but you'll usually want to use one of the others for floating point data. Average is a good choice for this case.\n",
    "\n",
    "See http://www.gdal.org/gdaladdo.html for more info."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build pyramid levels.\n",
    "levels = [2, 4, 8, 16, 32]\n",
    "ndvi_ds.BuildOverviews('average', levels)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can close the dataset by deleting the variable that holds it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "del ndvi_band, ndvi_ds, ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Everything together\n",
    "\n",
    "This is what it looks like all together instead of broken out with explanations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from osgeo import gdal\n",
    "\n",
    "# Read the data\n",
    "ds = gdal.Open('aster.img')\n",
    "if ds is None:\n",
    "    raise IOError('Could not open raster')\n",
    "    \n",
    "red = ds.GetRasterBand(2).ReadAsArray()\n",
    "nir = ds.GetRasterBand(3).ReadAsArray().astype('int32')\n",
    "\n",
    "# Mask out the bad pixels\n",
    "mask = np.ma.equal(nir + red, 0)\n",
    "red = np.ma.masked_array(red, mask)\n",
    "\n",
    "# Do the math\n",
    "ndvi = (nir - red) / (nir + red)\n",
    "ndvi = ndvi.filled(-99)\n",
    "\n",
    "# Create a new dataset\n",
    "gtiff_driver = gdal.GetDriverByName('GTiff')\n",
    "ndvi_ds = gtiff_driver.Create('ndvi.tif', ds.RasterXSize, ds.RasterYSize, 1, gdal.GDT_Float32)\n",
    "if ndvi_ds is None:\n",
    "    raise IOError('Could not create raster')\n",
    "ndvi_ds.SetProjection(ds.GetProjection())\n",
    "ndvi_ds.SetGeoTransform(ds.GetGeoTransform())\n",
    "\n",
    "# Write the data\n",
    "ndvi_band = ndvi_ds.GetRasterBand(1)\n",
    "ndvi_band.WriteArray(ndvi)\n",
    "ndvi_band.SetNoDataValue(-99)\n",
    "\n",
    "# Compute stats\n",
    "ndvi_band.FlushCache()\n",
    "ndvi_band.ComputeStatistics(False)\n",
    "\n",
    "# Build pyramids\n",
    "levels = [2, 4, 8, 16, 32]\n",
    "ndvi_ds.BuildOverviews('average', levels)\n",
    "\n",
    "del ndvi_band, ndvi_ds, ds"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
