{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Choosing an appropriate data type\n",
    "\n",
    "You've seen how integers and floating point numbers can give different results in division operations, but there are also other factors to consider when choosing a data type. For example, you need to use an integer data type if you need an attribute table to be associated with your raster.\n",
    "\n",
    "### Storage space\n",
    "\n",
    "Another factor is how much space something takes up on disk. For example, a 1,000 x 1,000 byte dataset takes up 1000 x 1000 x 1 = 1,000,000 bytes, or 1 megabyte of space. A byte is eight bits, so 32 bits is four bytes. If you save that same 1,000 x 1,000 dataset as 32-bit float instead, it takes up 1000 x 1000 x 4 = 4,000,000 bytes, or 4 megabytes. With a tiny dataset like that, it probably doesn't make much difference, but what about a dataset that's half a gigabyte when stored as byte? If you stored it as 32-bit, it would take up 2 GB instead, and that difference in size might be important. \n",
    "\n",
    "Obviously, a lot of data can't be reduced to a byte, since that only supports values between 0 and 255, but the principle is the same. Don't use a larger data type than you need, unless you like wasting disk space (and memory, when it comes time to process the data).\n",
    "\n",
    "### Calculations\n",
    "\n",
    "Sometimes an appropriate storage data type is inappropriate for calculations. In fact, the division problem is an example of this. The ASTER satellite imagery you've been using to calculate NDVI and SAVI values with is stored as 8-bit (byte), but you needed to convert the data to a larger data type before you could do your calculations.\n",
    "\n",
    "Another example comes from a mistake I made when coming up with the sharpening problem (in the raster-processing notebook) a few years ago. When I first came up with that, I didn't think to convert the data to another data type because there was no division involved (which is an important point with Python 2.7 but not so much with Python 3, like Pro uses) and the elevation values are large so I knew the image wasn't stored as 8-bit. Unfortunately, I didn't give it any thought beyond those two facts, and assumed that whatever data type was used in the elevation image was appropriate for the calculations. It wasn't, but why?\n",
    "\n",
    "Originally I thought that the output should have values in the range 0 to 65,535 and look like this:\n",
    "\n",
    "![bad](images/sharpen-old.png)\n",
    "\n",
    "However, the value range if you do the problem *correctly* is -107 to 126 and the output looks like this:\n",
    "\n",
    "![good](images/sharpen.png)\n",
    "\n",
    "That's quite the difference, isn't it? The only thing I did wrong was leave the data type as it was (`uint16`), but how could that make such a drastic difference in the result?\n",
    "\n",
    "The 'u' in stands for 'unsigned', which means it can't hold negative numbers. Therefore, if the correct result of the calculation was a negative number, it wrapped around and was subtracted from the upper bound of possible `uint16` values. For example, if the answer should've been -25, it turned it into 65,535 - 25 = 65,511. Crazy, huh?\n",
    "\n",
    "Here's a concrete example. First let's define a function that's similar to the `sharpen` function from the raster-processing notebook. The difference is that this one returns an array containing one number of the same data type as the array that was passed in. This is needed because the original function works the way we expect when used with these examples, even though it doesn't when used with a filter. This modified function behaves more like the original one does when it's used with a filter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sharpen(data):\n",
    "    \"\"\"Calculates slope using a 3x3 window.\n",
    "\n",
    "    data - 1D array containing the 9 pixel values, starting\n",
    "           in the upper left and going left to right and down\n",
    "    \"\"\"\n",
    "    return np.array([8 * data[4] - data[:4].sum() - data[5:].sum()]).astype(data.dtype)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now create a numpy array containing nine numbers. These are actual values that could be passed to the `sharpen` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "data = np.array([1797, 1799, 1801, 1801, 1801, 1800, 1810, 1808, 1806])\n",
    "data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use the `dtype` property to see what data type numpy used to create the array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data.dtype"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That array is 32-bit, so it should work correctly. Pass it to `sharpen` and see what you get:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sharpen(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So far, so good.\n",
    "\n",
    "Now convert the array to unsigned 16-bit."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data16 = data.astype('uint16')\n",
    "data16.dtype"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The numbers still look the same:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data16"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But when you pass this array to `sharpen`, check out what happens:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sharpen(data16)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Whoa! What happened to -14?\n",
    "\n",
    "Because you used an unsigned data type, a negative number isn't possible, so the real answer of -14 can't be represented. So it subtracted that from the maximum value for a 16-bit integer. Here's the maximum value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "2**16"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here it is with 14 subtracted from it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "2**16 - 14"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That number should look familiar, because it's the same answer you got from `sharpen` when you passed it `data16`.\n",
    "\n",
    "The take-home message here is that you should make sure you're using an appropriate data type for your calculations (and that you check your result carefully to make sure it makes sense!)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
