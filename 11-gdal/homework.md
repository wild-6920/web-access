# GDAL homework

**Due March 29 at midnight**

## Notebook comprehension questions

*Worth 11 points*

Work through the notebooks in this order:

- 1-raster-basics.ipynb (4 points)
- 2-raster-processing.ipynb (6 points)
- 3-data-types.ipynb (1 points)

## Script problems

*Worth 10 points each*

There's only two problems this week because the other one I usually assign at this point assumes you know how to use the vector part of GDAL, but I decided to skip that for now in favor of showing you GeoPandas at some point. And I'm too tired and/or lazy to come up with a third problem. Somehow I don't think you'll mind!


## Problem 1

Create a notebook called `problem1`. Set up five variables in the very first cell, with these names and values (except change the path to the data folder on **your** computer:

```python
input_filename = r'D:\classes\WILD6920\Spring2020\python-gis\11-gdal\data\aster.img'
output_filename = r'D:\classes\WILD6920\Spring2020\python-gis\11-gdal\data\savi.tif'
L = 0.5
```

Give this cell a `parameters` tag.

1\. Calculate a [Soil Adjusted Vegetation Index](https://en.wikipedia.org/wiki/Soil-Adjusted_Vegetation_Index) (SAVI) for `aster.img`. SAVI is another vegetation index, like NDVI, but this one attempts to adjust for different soils and the way they reflect data back to the sensor.

Here's the equation:

![savi equation](images/savi-eq.png)

where `NIR` is the near-infrared band (band 3), `Red` is the red band (band 2), and `L` is a correction factor.

2\. After you've calculated your savi, plot it to make sure it looks okay. Normalize it between -1.5 and 1.5 and use a grayscale. It should look something like this:

![savi](images/savi.png)

3\. Now create an output GeoTIFF file, write the data to the output band, set the NoData value, and calculate and print the band statistics. They should match this (I rounded them to 4 digits, but you don't have to):

- Min: -1.496
- Max: 1.4942
- Mean: 0.077
- Standard deviation: 0.2377

4\. Make sure you set the geotransform and spatial reference and build pyramids on your result. You can use pyramid levels [2, 4, 8, 16, 32]. 

5\. Don't forget to close the files by deleting the variables.

6\. Make sure that `check_homework.py` is in the same folder as your notebook and call the `problem1()` function in that file. You'll have to import it as a module first, and pass `output_filename` to the function when you call it. It'll do some basic checking on your output file. (`check_homework.py` is in this week's folder.)

7\. Make sure that your notebook runs if you restart the kernel and that you've used the variables from the first cell rather than typing out the filenames or the `L` value later on.

I found these questions that I had used several years ago. See if they're useful to you as you tackle the homework.

**Questions to ask yourself**

1. Which modules need to be imported?
2. How do I set things up so plots will show in the notebook?
2. Do I have full file paths? Is there a need to set the working folder?
2. Which bands do I need from the aster image?
3. Do I need to convert anything to floating point in order to make the math work correctly?
4. What condition would cause a division by zero error and how do I account for this?
5. How do I calculate the SAVI?
6. Which driver do I need in order to create the output raster?
7. What information do I need from the aster image in order to create a new raster with the same numbers of rows and columns? How do I get this information?
8. How many bands does the output raster need?
9. What data type should the output raster have?
10. How do I make sure that the output raster has the correct spatial reference?
11. How do I make sure that the output raster is georeferenced correctly?
12. How do I make sure that the output raster knows what value to use as NoData?
12. How do I write the SAVI data to the output raster?
13. How do I calculate statistics?
14. How do I build pyramids? 



## Problem 2

Create a notebook called `problem2`. Set up five variables in the very first cell, with these names and values (except change the path to the data folder on **your** computer:

```python
folder = r'D:\classes\WILD6920\Spring2020\python-gis\11-gdal\data'
b1_filename = 'aster_clip_1.tif'
b2_filename = 'aster_clip_2.tif'
b3_filename = 'aster_clip_3.tif'
output_filename = 'nir_composite.tif'
```

Give this cell a `parameters` tag.

1\. Combine the three aster_clip images into one 3-band image with bands in 321 order, meaning that aster band 3 will be band 1 in your new image. They all have the same number of rows and columns, projection, geotransform, etc. All of the examples in the notebook created a single band image, but if you look at the description in the first example, it tells you how to create rasters with more bands.

Make sure you create the output image with the same data type as the originals. Use one of the band's `DataType` property (e.g. `myband.DataType`) to get the appropriate data type to use when you create the new raster. The new image should also have the same geotransform and spatial reference system as the originals.

2\. The 321 order is a near-infrared color composite, so vegetation appears red. Plot it in your notebook like this (PIL is an image processing module and is an easy way to display the TIFF in the notebook):

```python
from PIL import Image
Image.open(output_filename) 
```

It should look like this:

![composite](images/clipped_aster.png)

Also try opening it with the default Windows viewer by double-clicking on it. If that won't open it, then you didn't save it with the correct data type.

3\. Don't forget to close the files by deleting the variables.

4\. Make sure that `check_homework.py` is in the same folder as your notebook and call the `problem2()` function in that file. You'll have to import it as a module first, and pass `output_filename` to the function when you call it. It'll do some basic checking on your output file. (`check_homework.py` is in this week's folder.)

5\. Make sure that your notebook runs if you restart the kernel and that you've used the variables from the first cell rather than typing out the filenames later on.


**Questions to ask yourself**

1. Which modules need to be imported?
2. Do I have full file paths? Is there a need to set the working folder?
2. Which rasters do I need to get data from?
3. Which driver do I need in order to create the output raster?
4. What information do I need from one of the input images in order to create a new raster with the same numbers of rows and columns? How do I get this information?
5. How many bands does the output raster need?
6. What information do I need from one of the input images in order to create a new raster with the same data type? How do I get this information?
7. How do I make sure that the output raster has the correct spatial reference?
8. How do I make sure that the output raster is georeferenced correctly?
9. Which input raster needs to go in which output band?
10. How do I read the data in from one of the input rasters?
11. How do I insert the data into the correct band in the output raster?

