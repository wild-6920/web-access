{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Notebook setup\n",
    "\n",
    "**Don't change the data folder.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_folder = 'data'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "from osgeo import gdal\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Change the Python working directory.\n",
    "os.chdir(data_folder)\n",
    "\n",
    "# Set up matplotlib to draw in the notebook.\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Convert real-world coordinates to pixel offsets\n",
    "\n",
    "Sometimes you don't need the entire dataset and it makes sense to only read in what you need. Let's look at this with individual pixels, but a similar concept applies to working with larger sections. \n",
    "\n",
    "Note that if your dataset isn't very large, you'll get better performance by reading the entire thing in than by accessing each pixel individually.\n",
    "\n",
    "The easiest way to figure out which pixels you want based on real-world coordinates is to use the gdal [`ApplyGeoTransform`](../osgeo_docs/osgeo.gdal.html#-ApplyGeoTransform) function along with the raster's geotransform and your coordinates. You briefly saw the geotransform in the last notebook, so maybe you remember that it contains info about upper left coordinates and pixel sizes. \n",
    "\n",
    "When used with `ApplyGeoTransform()`, a regular geotransform converts pixel offsets to real-world coordinates, but if you want to go from real-world to pixel you need an *inverse* geotransform. You can get one of these with the `InvGeoTransform()` function, which returns a new geotransform or `None` if one couldn't be calculated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open aster.img and get its geotransform.\n",
    "ds = gdal.Open('aster.img')\n",
    "geotransform = ds.GetGeoTransform()\n",
    "\n",
    "# Get the inverse geotransform, which will help convert real-world coordinates to pixel offsets.\n",
    "inverse_geotransform = gdal.InvGeoTransform(geotransform)\n",
    "if inverse_geotransform is None:\n",
    "    raise RuntimeError('Could not calculate inverse geotransform')\n",
    "\n",
    "# Print the result.\n",
    "print('original:', geotransform)\n",
    "print('inverse:', inverse_geotransform)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can convert real-world coordinates to pixel offsets by passing them, along with the inverse geotransform, to the `ApplyGeoTransform()` function. This'll return the column and row offsets that correspond to the coordinates. Go ahead and get the offsets for the cell at these UTM coordinates: \n",
    "\n",
    "- x: 470000 \n",
    "- y: 4600000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use the inverse geotransform to get pixel offsets for UTM coordinates.\n",
    "x, y = gdal.ApplyGeoTransform(inverse_geotransform, 470000, 4600000)\n",
    "print(x, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These are the pixel offsets you would use to get a value out of a NumPy array....*almost*. If you try to use floating point numbers, Numpy will get upset. So you need to convert them to integers so you can get the value at row 3334 and column 4161 (don't round, because that could bump you off a cell)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = int(x)\n",
    "y = int(y)\n",
    "print(x, y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Read just one pixel at a time\n",
    "\n",
    "The [`ReadAsArray`](../osgeo_docs/osgeo.gdal.html#Band-ReadAsArray) method has some optional parameters that will let you read in exactly the data you want instead of the entire band.\n",
    "\n",
    "```\n",
    "<band>.ReadAsArray(x_offset, y_offset, win_xsize, win_ysize, buf_xsize, buf_ysize, buf_obj)\n",
    "```\n",
    "\n",
    "We're only going to use the first four parameters. `x_offset` and `y_offset` are the column and row offsets to begin reading at, and `win_xsize` and `win_ysize` are the number of columns and rows to read. So to get just one pixel, you would do something like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the first band from the raster.\n",
    "band = ds.GetRasterBand(1)\n",
    "\n",
    "# Read in 1 column and 1 row, starting at column x and row y.\n",
    "data = band.ReadAsArray(x, y, 1, 1)\n",
    "\n",
    "# Print the result.\n",
    "print(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`ReadAsArray` always returns a two-dimensional array, even if you only asked for one pixel. So to get the actual value, get the value at position `0,0` (that's row, column) in the array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "value = data[0, 0]\n",
    "value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "## Problem 1\n",
    "\n",
    "Read in the pixel value at row 2000 (y) and column 1000 (x) and store it in a variable called `p1`. Make sure you get the number out of the array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now check your result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": [
    "assert type(p1) == np.uint8, 'not an integer'\n",
    "assert p1 == 98, 'wrong pixel value'\n",
    "print('looks good')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Get values from a NumPy array\n",
    "\n",
    "What if you had read the entire band into an array already? (This would be the most efficient in some cases.) Then just use the offsets to pull the value right out of the array. **Just remember that NumPy arrays use [row, column], which is equivalent to [y, x].**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read in all pixel values from the band.\n",
    "data = band.ReadAsArray()\n",
    "\n",
    "# Get the pixel value at row y and column x.\n",
    "value = data[y, x]\n",
    "\n",
    "# Print the result.\n",
    "value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "## Problem 2\n",
    "\n",
    "Get the pixel at row 2000 and column 1000 from the `data` array and store it in a variable called `p2`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This checks your result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": [
    "assert type(p2) == np.uint8, 'not an integer'\n",
    "assert p2 == 98, 'wrong pixel value'\n",
    "print('looks good')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also read part of the raster into a two-dimensional numpy array using the same technique you used to read a single pixel, and then use offsets to get data out of that array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read in 3 columns and 5 rows, starting at column x and row y.\n",
    "data = band.ReadAsArray(x, y, 3, 5)\n",
    "print(data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, you can get one value out of this array by using the (row, column) offsets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the value in the 3rd row from the top and 2nd column from the left.\n",
    "data[2, 1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to get an entire row out of this array, you can just use the row index:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data[2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get a column, use a colon for the row index and then provide a column index."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data[:, 1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That didn't print out vertically becuase now it's a 1-dimensional array, but if you compare the numbers you'll see that it's the numbers from the second column.\n",
    "\n",
    "If you want to get a subset of this array, you can give starting and ending offsets for rows and columns, with the start and end separated by a colon, and the rows and columns still separated by a comma. The ending offset isn't included in the output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the second through fourth rows, and the second and third columns.\n",
    "print(data[1:4, 1:3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to start at the beginning or go to the end, you don't have to provide the starting or ending offset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the first three rows and the last two columns.\n",
    "print(data[:3, 1:])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "## Problem 3\n",
    "\n",
    "Read in 3 columns and 4 rows, starting at row 2000 and column 1000 and store the result in a variable called `p3`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This checks your result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": [
    "assert type(p3) == np.ndarray, 'not an array'\n",
    "assert p3.shape == (4, 3), 'wrong number of rows and columns'\n",
    "assert np.array_equal(p3, [[98, 100, 102], [99, 98, 97], [97, 95, 94], [92, 92, 92]]), 'wrong values'\n",
    "print('looks good')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Reclassification\n",
    "\n",
    "Let's look at a few more examples of using raster data that are a bit more complicated than map algebra.\n",
    "\n",
    "How about the same elevation classification that we did in the Esri part of the class? Here are some elevation ranges and new pixel values:\n",
    "\n",
    "\n",
    "| Range | New value |\n",
    "|-------|-----------|\n",
    "| 1500 - 2000 | 1 |\n",
    "| 2000 - 2500 | 2 |\n",
    "| 2500 - 3000 | 3|\n",
    "\n",
    "If you have programming experience in some other languages, you might be tempted to use a couple of `for` loops to iterate through all pixels and then use an `if` statement to figure out the new value. *Don't do that in Python!* It's much too slow. Instead, use built-in NumPy functions.\n",
    "\n",
    "First let's open the elevation dataset, get the band, and see what it looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the raster.\n",
    "in_ds = gdal.Open('cach_ned10_clip_int.img')\n",
    "\n",
    "# Get the first band and read all of the pixel values into the in_data array.\n",
    "in_data = in_ds.GetRasterBand(1).ReadAsArray()\n",
    "\n",
    "# Plot the band.\n",
    "plt.imshow(in_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now create an empty output dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a new GeoTIFF that's the same size as the elevation raster.\n",
    "out_ds = gdal.GetDriverByName('GTiff').Create('reclass.tif', in_ds.RasterXSize, in_ds.RasterYSize)\n",
    "\n",
    "# Copy the spatial reference and geotransform info from the elevation raster \n",
    "# to the new raster.\n",
    "out_ds.SetProjection(in_ds.GetProjection())\n",
    "out_ds.SetGeoTransform(in_ds.GetGeoTransform())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are several ways you could go about doing the reclassification, but let's look at one that's easy to understand. This requires that you create an array of the appropriate size first, though. Since the output raster will have the same number of rows and columns as the DEM, you can create an appropriate array like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create an empty integer array (np.int) with the\n",
    "# same dimensions as the in_data array (in_data.shape).\n",
    "out_data = np.empty(in_data.shape, np.int)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now `out_data` is an integer array with the same [shape](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.shape.html) (rows and columns) as `in_data`. The [empty](http://docs.scipy.org/doc/numpy/reference/generated/numpy.empty.html) function creates an array that is full of garbage, so you need to make sure you fill all of the pixels with something. If you'd rather initialize the array to 0 you could use [zeros](http://docs.scipy.org/doc/numpy/reference/generated/numpy.zeros.html).  [ones](http://docs.scipy.org/doc/numpy/reference/generated/numpy.ones.html) will initialize to 1, or you could use [full](http://docs.scipy.org/doc/numpy/reference/generated/numpy.full.html) to initialize to some other number.\n",
    "\n",
    "Now that you have an empty array, you can use [where(condition, true, false)](http://docs.scipy.org/doc/numpy/reference/generated/numpy.where.html) to reclassify the elevation values. The first parameter is a condition to check, the second is the value to put in each cell if the condition is true for that cell, and the second is the value to use when the condition is false. This is basically an `if-then-else` statement.\n",
    "\n",
    "You're going to use [logical_and](http://docs.scipy.org/doc/numpy/reference/generated/numpy.logical_and.html) to check each elevation range in `in_data` because it will allow you to compare against a minimum and a maximum value. \n",
    "\n",
    "For each elevation range, you'll check for pixels that fall in the range. The ones that do will get the new value, and the ones that don't will get the existing (`out_data`) value. You need to use `out_data` as the False option or else you would overwrite pixels that you had already set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# out_data gets a 1 everywhere that in_data >= 1500 and in_data < 2000,\n",
    "# otherwise it keeps the value it already has\n",
    "out_data = np.where(np.logical_and(in_data >= 1500, in_data < 2000), 1, out_data)\n",
    "\n",
    "# Repeat for the other two elevation ranges\n",
    "out_data = np.where(np.logical_and(in_data >= 2000, in_data < 2500), 2, out_data)\n",
    "out_data = np.where(np.logical_and(in_data >= 2500, in_data < 3000), 3, out_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's what your `out_data` array looks like now. It only has three values, so it only has three colors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.imshow(out_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now go ahead and create a new dataset and write out the new pixel values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a dataset the same size as the input.\n",
    "out_ds = gdal.GetDriverByName('GTiff').Create('reclass.tif', in_ds.RasterXSize, in_ds.RasterYSize)\n",
    "\n",
    "# Copy the projection and geotransform.\n",
    "out_ds.SetProjection(in_ds.GetProjection())\n",
    "out_ds.SetGeoTransform(in_ds.GetGeoTransform())\n",
    "\n",
    "# Write the data and calculate statistics.\n",
    "out_band = out_ds.GetRasterBand(1)\n",
    "out_band.WriteArray(out_data)\n",
    "out_band.ComputeStatistics(False)\n",
    "\n",
    "# Close the file.\n",
    "del out_ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want, you can make sure that the data really got written okay by reading it back in and plotting it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = gdal.Open('reclass.tif')\n",
    "plt.imshow(ds.GetRasterBand(1).ReadAsArray())\n",
    "del ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's create a simple function that plots the first band in a raster image using default options. This'll make it easier to plot some of your outputs later on. The `**kwargs` stuff allows the function to accept other keyword arguments that it passes straight to `imshow()` without caring what they are. That'll let you pass extra arguments, like `norm`, to your `plot_raster()` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_raster(filename, **kwargs):\n",
    "    ds = gdal.Open(filename)\n",
    "    plt.imshow(ds.GetRasterBand(1).ReadAsArray(), **kwargs)\n",
    "    del ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's another way to do the reclassification using [select](http://docs.scipy.org/doc/numpy/reference/generated/numpy.select.html), where you create a list of conditions and a list of results. If the first condition is True, then the output gets the first item from the list of results. If the second condition is True, then the output gets the second item from the list of results. You don't need to initialize an output array when you do it this way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the elevation raster and read the data into an array.\n",
    "in_ds = gdal.Open('cach_ned10_clip_int.img')\n",
    "in_data = in_ds.GetRasterBand(1).ReadAsArray()\n",
    "\n",
    "# Create the condition list.\n",
    "conditions = [\n",
    "    np.logical_and(in_data >= 1500, in_data < 2000),\n",
    "    np.logical_and(in_data >= 2000, in_data < 2500),\n",
    "    np.logical_and(in_data >= 2500, in_data < 3000)\n",
    "]\n",
    "                   \n",
    "# Create the list of results for each condition.\n",
    "results = [1, 2, 3]\n",
    "\n",
    "# Do the work. If a pixel meets a condition in the conditions list, then\n",
    "# the output gets the corresponding value from the results list.\n",
    "out_data = np.select(conditions, results)\n",
    "\n",
    "# Save the data.\n",
    "out_ds = gdal.GetDriverByName('GTiff').Create('reclass2.tif', in_ds.RasterXSize, in_ds.RasterYSize)\n",
    "out_ds.SetProjection(in_ds.GetProjection())\n",
    "out_ds.SetGeoTransform(in_ds.GetGeoTransform())\n",
    "out_band = out_ds.GetRasterBand(1)\n",
    "out_band.WriteArray(out_data)\n",
    "out_band.ComputeStatistics(False)\n",
    "del out_ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can make sure it worked by plotting it with the function you wrote a minute ago:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_raster('reclass2.tif')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "## Problem 4\n",
    "\n",
    "Change the code below so that it reclassifies using these ranges and values:\n",
    "\n",
    "| Elevation range | New value |\n",
    "|-------|-----------|\n",
    "| 1500 - 1750 | 1 |\n",
    "| 1750 - 2000 | 2 |\n",
    "| 2000 - 2250 | 3 |\n",
    "| 2250 - 2500 | 4 |\n",
    "| 2500 - 3000 | 5 |"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": [
    "# Create the condition list.\n",
    "conditions = [\n",
    "    np.logical_and(in_data >= 1500, in_data < 2000),\n",
    "    np.logical_and(in_data >= 2000, in_data < 2500),\n",
    "    np.logical_and(in_data >= 2500, in_data < 3000)\n",
    "]\n",
    "\n",
    "# Create the list of results for each condition.\n",
    "results = [1, 2, 3]\n",
    "\n",
    "# Do the work. If a pixel meets a condition in the conditions list, then\n",
    "# the output gets the corresponding value from the results list.\n",
    "p4 = np.select(conditions, results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It should plot out with five colors now:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": [
    "# Plot the result\n",
    "plt.imshow(p4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Timing your code\n",
    "\n",
    "Obviously there are lots of ways to do things. If you're using large datasets, speed matters. Let's see which of these two methods runs faster. To do that, we can use the IPython `%%timeit` magic command, which works in Jupyter notebooks (but is not a normal Python command).\n",
    "\n",
    "This cell times the first method. It's only doing the processing part, not the part that writes data to disk, because that's the same with either method. This will take a while to run because it runs the code a bunch of times in order to get a good estimate of how long it takes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit\n",
    "test_data = np.empty(in_data.shape, np.int)\n",
    "test_data = np.where(np.logical_and(in_data >= 1500, in_data < 2000), 1, test_data)\n",
    "test_data = np.where(np.logical_and(in_data >= 2000, in_data < 2500), 2, test_data)\n",
    "test_data = np.where(np.logical_and(in_data >= 2500, in_data < 3000), 3, test_data)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once that finishes, you should see some timing info under the cell. The smaller the number, the better.\n",
    "\n",
    "Now let's try the other method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%timeit\n",
    "conditions = [\n",
    "    np.logical_and(in_data >= 1500, in_data < 2000),\n",
    "    np.logical_and(in_data >= 2000, in_data < 2500),\n",
    "    np.logical_and(in_data >= 2500, in_data < 3000)\n",
    "]\n",
    "results = [1, 2, 3]\n",
    "out_data = np.select(conditions, results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On my laptop, the second method came out at about 12 milliseconds while the first one was about 19 milliseconds. So if I was doing this on a huge dataset, I'd want to use the second method. It doesn't seem like much here (because it's not!) but it can definitely add up with huge amounts of data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Moving windows\n",
    "\n",
    "You can use NumPy to implement moving windows, but it's easier to use SciPy, which is another Python module that's designed for scientific computing. You're in luck, because it gets installed with ArcGIS.\n",
    "\n",
    "Let's try out a smoothing filter that calculates the average value of each set of 9 pixels in a 3x3 window. You did this in the Esri part of the class, but if you've forgotten about it, you can see the [Esri Filter](https://pro.arcgis.com/en/pro-app/tool-reference/spatial-analyst/how-filter-works.htm) documentation for a good explanation of what you're doing here. This is an example of a low pass filter.\n",
    "\n",
    "You'll use the SciPy [uniform_filter](http://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.uniform_filter.html) function to smooth the NDVI image you just made. The one required parameter is the array to filter. You'll also use `size` to set the size of the moving window and `mode` to specify how the edge pixels are handled. At the edges of the image there isn't enough data for a full-sized window, but one is simulated using the method provided with the `mode` argument. A mode of 'nearest' tells it to use the nearest valid value when filling in placeholder values along the edges. The moving windows then use these placeholders as real values when calculating edge outputs. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import the required part of SciPy.\n",
    "import scipy.ndimage\n",
    "\n",
    "# Open the NDVI raster and read the data into a numpy array.\n",
    "ndvi_ds = gdal.Open('ndvi.tif')\n",
    "ndvi_data = ndvi_ds.GetRasterBand(1).ReadAsArray()\n",
    "\n",
    "# This is the part that does the moving window stuff. It operates on ndvi_data, uses a 3x3 window\n",
    "# to calculate average pixel values, and uses the nearest valid pixel value when it's at the edge \n",
    "# of the array and doesn't have a full 3x3 window available.\n",
    "smooth_data = scipy.ndimage.uniform_filter(ndvi_data, size=3, mode='nearest')\n",
    "\n",
    "# Save the data.\n",
    "smooth_ds = gdal.GetDriverByName('GTiff').Create('smoothed.tif', ndvi_ds.RasterXSize, ndvi_ds.RasterYSize, 1, gdal.GDT_Float32)\n",
    "smooth_ds.SetProjection(ndvi_ds.GetProjection())\n",
    "smooth_ds.SetGeoTransform(ndvi_ds.GetGeoTransform())\n",
    "smooth_band = smooth_ds.GetRasterBand(1)\n",
    "smooth_band.WriteArray(smooth_data)\n",
    "smooth_band.ComputeStatistics(False)\n",
    "del smooth_ds, ndvi_ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here's your output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_raster('smoothed.tif', norm=plt.Normalize(-1, 1), cmap='gray')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's hard to tell from that if it's been smoothed at all, so let's try reading in a 3x3 square from somewhere in the image and compare it to the original from the same location.\n",
    "\n",
    "Here's some data from the smoothed image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = gdal.Open('smoothed.tif')\n",
    "print(ds.GetRasterBand(1).ReadAsArray(4000, 4000, 3, 3))\n",
    "del ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And here's data from the same location in the original, as well as its mean. Since you ran a simple averaging filter, you know that its mean should be equal to the middle pixel in the smoothed data you just printed out, or -0.109103."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds = gdal.Open('ndvi.tif')\n",
    "data = ds.GetRasterBand(1).ReadAsArray(4000, 4000, 3, 3)\n",
    "del ds\n",
    "\n",
    "print(data)\n",
    "print('mean:', data.mean())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The mean is what we expected!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Custom moving windows\n",
    "\n",
    "You can also use your own equations in a moving window. Here is a function that calculates slope for a 3x3 window, based on the equations found [here](http://desktop.arcgis.com/en/desktop/latest/tools/spatial-analyst-toolbox/how-slope-works.htm). The pixels in the 3x3 array get passed to the function with these indices:\n",
    "\n",
    "```\n",
    "0  1  2\n",
    "3  4  5\n",
    "6  7  8\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def slope(data, cell_width, cell_height):\n",
    "    \"\"\"Calculates slope using a 3x3 window.\n",
    "\n",
    "    data        - 1D array containing the 9 pixel values, starting\n",
    "                  in the upper left and going left to right and down\n",
    "    cell_width  - pixel width in the same units as the data\n",
    "    cell_height - pixel height in the same units as the data\n",
    "    \"\"\"\n",
    "    rise = ((data[6] + (2 * data[7]) + data[8]) -\n",
    "            (data[0] + (2 * data[1]) + data[2])) / \\\n",
    "           (8 * cell_height)\n",
    "    run =  ((data[2] + (2 * data[5]) + data[8]) -\n",
    "            (data[0] + (2 * data[3]) + data[6])) / \\\n",
    "           (8 * cell_width)\n",
    "    dist = np.sqrt(np.square(rise) + np.square(run))\n",
    "    return np.arctan(dist) * 180 / np.pi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now you can use the [generic_filter](http://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.generic_filter.html) function to apply the `slope()` function to the elevation dataset. We'll use these parameters:\n",
    "\n",
    "- `input`: The numpy array to apply the filter on (required). We'll read in an elevation dataset for this.\n",
    "- `function`: The function to apply (required). This is the `slope()` function we just made.\n",
    "- `size`: The size of the moving window.\n",
    "- `mode`: The method for dealing with edge pixels.\n",
    "- `extra_arguments`: Any extra parameters, other than the pixel values, that are required by the function being used. These are passed in as a tuple. These are the parameters required by `slope()`, other than the data array-- so `cell_width` and `cell_height`.\n",
    "\n",
    "You'll convert the elevation values to a larger data type when you read them in, just to be safe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Open the elevation raster and read the data in.\n",
    "in_ds = gdal.Open('cach_ned10_clip_int.img')\n",
    "in_data = in_ds.GetRasterBand(1).ReadAsArray().astype('int32')\n",
    "\n",
    "# Cell width and height are available from the geotransform. You need them\n",
    "# for the slope() function.\n",
    "gt = in_ds.GetGeoTransform()\n",
    "cell_width = gt[1]\n",
    "cell_height = gt[5]\n",
    "\n",
    "# This is the part that does the work. It applies the slope() function to the in_data\n",
    "# array using a 3x3 window and the nearest valid values at the edges. The slope() \n",
    "# function requires cell width and cell height, so the `extra_arguments` parameter is\n",
    "# used to pass those values on to slope().\n",
    "out_data = scipy.ndimage.generic_filter(\n",
    "    in_data, slope, size=3, mode='nearest', extra_arguments=(cell_width, cell_height))\n",
    "\n",
    "# Save the data.\n",
    "out_ds = gdal.GetDriverByName('GTiff').Create('slope.tif', in_ds.RasterXSize, in_ds.RasterYSize, 1, gdal.GDT_Int32)\n",
    "out_ds.SetProjection(in_ds.GetProjection())\n",
    "out_ds.SetGeoTransform(in_ds.GetGeoTransform())\n",
    "out_band = out_ds.GetRasterBand(1)\n",
    "out_band.WriteArray(out_data)\n",
    "out_band.ComputeStatistics(False)\n",
    "del out_ds, in_ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `generic_filter()` function goes through the image and for each 3x3 window, passes those 9 pixels to the function you specified as the second argument (`slope`, in this case). If the custom function requires parameters in addition to the pixel values, you can pass them with the `extra_arguments` parameter to `generic_filter()`.\n",
    "\n",
    "Here's what your output slope raster looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_raster('slope.tif')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember the sharpening filter from the Esri part of the class? The weights looked like this:\n",
    "\n",
    "```\n",
    "-1  -1  -1\n",
    "-1   8  -1\n",
    "-1  -1  -1\n",
    "```\n",
    "Here's a function that would implement that weighting scheme:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def sharpen(data):\n",
    "    \"\"\"Calculates slope using a 3x3 window.\n",
    "\n",
    "    data - 1D array containing the 9 pixel values, starting\n",
    "           in the upper left and going left to right and down\n",
    "    \"\"\"\n",
    "    return 8 * data[4] - data[:4].sum() - data[5:].sum()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`data[4]` is the middle cell, so it's multiplied by 8. `data[:4]` is the first four cells and `data[5:]` are the last four cells. They're just summed up and subtracted from the multiple of the middle cell.\n",
    "\n",
    "For more information about the available SciPy filters, see [Multidimensional image processing](http://docs.scipy.org/doc/scipy/reference/tutorial/ndimage.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hw": true
   },
   "source": [
    "### Problem 5\n",
    "\n",
    "Apply the `sharpen()` function just created to the elevation raster. Use a 3x3 window and `mode='nearest'`. The code to read the data in is provided for you. Save the filter result in a variable called `p5`. You don't need to save it to a file unless you want to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": [
    "# Open the elevation raster and read the data.\n",
    "in_ds = gdal.Open('cach_ned10_clip_int.img')\n",
    "in_data = in_ds.GetRasterBand(1).ReadAsArray().astype('int32')\n",
    "\n",
    "# Your code to apply sharpen to the elevation raster goes here.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This'll check to see if you have the correct min and max values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": [
    "assert p5.min() == -107 and p5.max() == 126, 'wrong min and max values, so something went wrong'\n",
    "print('looks good')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This next bit of code uses a standard deviation stretch to plot your result. It should look like the example shown below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hw": true
   },
   "outputs": [],
   "source": [
    "# Create a standard deviation stretch so that the details are visible\n",
    "std = p5.std()\n",
    "mean = p5.mean()\n",
    "stretch = plt.Normalize(mean - 2 *std, mean + 2 * std, True)\n",
    "\n",
    "# Draw the figure larger than default\n",
    "plt.figure(figsize = (10,10))\n",
    "plt.imshow(p5, cmap='gray', aspect='equal', norm=stretch)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your plot should look similar to this:\n",
    "\n",
    "![sharpen](images/sharpen.png)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
